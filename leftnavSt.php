
<?php
    //include "checkIn.php";
    include "checkcode.php";
?>
<html>

<head>
<meta http-equiv="refresh" content="300;url=logout.php" />

        <title>SmartAcademy</title>
        <link rel="stylesheet" type="text/css" href="../css/animate/animate.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/msg.css">

        <link rel="stylesheet" href="../css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/navstyle.css">
        <link rel="stylesheet" href="../css/fontawesome-free/css/all.css">
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../css/bootstrap4/js/bootstrap.min.js"></script>
        <script src="js/sweetalert.min.js"></script>
        <script>
            $(document).ready(function () {

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

            });
        </script>
        
</head>
<body>
        <?php include "studNav.php" ?> 
        <div class="wrapper1 ">
            <div class="row mt-3 pb-5">
                    <!-- Sidebar -->
                        <nav id="sidebar" class="col-md-3"><br>
                            
                    
                            <ul class="list-unstyled components">
                                
                                
                                <li class="pl-2">
                                <?php include "reg.php"; ?>
                                
                                   
                                </li>
                                <li>
                                    <a class="nav-link" href="studentsNew.php?label=enrlUnits">
                                        Units You Have Enrolled For
                                    </a>
                                </li>
                                <li>
                                    <a href="studentsNew.php?label=stUnits">
                                        Click To Enroll For More Units 
                                    </a>
                                </li> 


                                
                            </ul>
                        </nav> 
                    <div class="col-md-9 ml-auto mr-auto">
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <button type="button" id="sidebarCollapse" class="btn btn-info">
                                        <i class="fas fa-align-left"></i>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                                <div class="container-fluid">