<!DOCTYPE html>
<html>
	<head>
		<script src="../js/graf.js"></script>
		<script src="../js/grafJquery.js"></script>
		<style>
		 .para{
            /* writing-mode:tb-rl; */
			font-size:17px;
			color:#006600;
			font-family:Trattatello;
         }
		 .unitspg{
			 background-color:white;
		 }
		</style>
    </head>
	<body>
		<form METHOD="POST">
			<div class="unitspg pt-1 pr-2 pl-2 pb-3 mt-5">
				<div class="row justify-content-center">
					<div class="para col-md-12 mb-2">
						
						Fill In <strong>Unit's System-Id </strong> To Compare How They Were Performed
						
					</div>
				</div>
				
				<div class="row mb-3 mr-1 justify-content-center">
					<div class="col-md-3 ">
						<input type="text" name="idunit" placeholder="enter unit-id">
					</div>

					<div class="col-md-3 ">
						<input type="text" name="idunit2" placeholder="enter unit-id">
					</div>

					<div class="col-md-3 ">
						<input type="text" name="idunit3" placeholder="enter unit-id">
					</div>

					<input type="submit" class="btn btn-sm btn-info" name="submit" value="SUBMIT"/>
					
				</div>

				<div class="row justify-content-center">
				
					<?php
					//session_start();
					if(isset($_POST['submit'])){
						$idunit = $_POST['idunit']; 
						$idunit2 = $_POST['idunit2']; 
						$idunit3 = $_POST['idunit3'];
							
					}else{
							$idunit = 'unit-0012'; 
							$idunit2 = 'unit-0012'; 
							$idunit3 = 'unit-0012';
					}
						include("databasecon.php");

						$_SESSION["idunit3"] ="$idunit3";
						$_SESSION["idunit2"] ="$idunit2";
						$_SESSION["idunit"] ="$idunit";
						
						//$_SESSION["unitid"] = $unitid1;
						$result = mysqli_query($connection, "SELECT * FROM grades where unitid='$idunit'  ");
						$result2 = mysqli_query($connection, "SELECT * FROM grades where unitid='$idunit2' ");
						$result3 = mysqli_query($connection, "SELECT * FROM grades where unitid='$idunit3' ");
						
						$row = mysqli_fetch_array($result);
						$row2 = mysqli_fetch_array($result2);
						$row3 = mysqli_fetch_array($result3);

						$unitname=$row['unitname'];
						$unitname2=$row2['unitname'];
						$unitname3=$row3['unitname'];

						$_SESSION["unitname"] = "$unitname";
						$_SESSION["unitname2"] = "$unitname2";
						$_SESSION["unitname3"] = "$unitname3";

						
						
						
						
					   ?>
															
								
					<head>
						<script type="text/javascript">
							$(document).ready(function () {

								$.getJSON("comparedata.php", function (result) {

									var chart = new CanvasJS.Chart("mychart", {
										data: [
											{
											type: "splineArea",
												dataPoints: result
											}
										]
									});

									chart.render();
								});
							});
						</script>
							
					</head>
								

					<div class="col-md-3 mr-5">

						<?php echo "UNIT-ID: "; echo  $_SESSION["idunit"] = $idunit;?>&nbsp;&nbsp;&nbsp;&nbsp;
						<?php    echo $unit1 = $_SESSION["unitname"];?>
						<br>
						<div id="mychart" style="width: 230px; height: 220px; float:left;">
						</div>
					</div>
	
											
					
					<head>
						<script type="text/javascript">
							$(document).ready(function () {

								$.getJSON("comparedata2.php", function (result) {

									var chart = new CanvasJS.Chart("chart", {
										data: [
											{
											type: "splineArea",
												dataPoints: result
											}
										]
									});

									chart.render();
								});
							});
						</script>
					</head>
								

					<div class="col-md-3 ml-4 mr-4">

						<?php echo "UNIT-ID: "; echo  $idunit2;?>&nbsp;&nbsp;&nbsp;&nbsp;
						<?php echo $unitname2?>
						<br>
						<div id="chart" style="width: 100%; height: 90%; float:left;"></div>
					</div>


					<head>
						<script type="text/javascript">
							$(document).ready(function () {

								$.getJSON("comparedata3.php", function (result) {

									var chart = new CanvasJS.Chart("chart3", {
										data: [
											{
											type: "splineArea",
												dataPoints: result
											}
										]
									});

									chart.render();
								});
							});
						</script>
					</head>
								

					<div class="col-md-3 ml-5">
						<P3><?php echo "UNIT-ID: "; echo  $idunit3;?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $unitname3?></P3>
						<br>
						<div id="chart3" style=" width: 100%; height: 90%; float:left;"></div>
					</div>
				</div>		
				
			</div>
				
				
		</form>
	</body>
</html>
