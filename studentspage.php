
<html>
    <head>
        <title>SmartAcademy</title>

        <link rel="stylesheet" href="../css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../css/bootstrap4/js/bootstrap.min.js"></script>

        <style>
        #leclnks{
            display: none;
            font-size: 20px;
            margin-top: 50px
        }
            
        </style>
    </head>
    <body>          
    <?php include "leftnavSt.php" ?>
            <div class="row  studentshome mt-2 justify-content-center" >
                <div class="col-md-11 "><h5><br> THANK YOU FOR BEING PART OF US</h5><br><br><br>
                    <h6>What To Expect:-</h6> 
                    <br>
                </div>
            
                <div class="col-md-3 mb-5">
                    <img src="../images/internet.jpg" width="100%" height="150px">
                    <h5 class="mt-3">Ease of access</h6>
                </div> 
            
                <div class="col-md-4 ">
                    <img src="../images/lock.jpg" width="150px" height="150px">
                    <h5 class="mt-3"> Security</h6>
                    
                </div>
            
                <div class="col-md-4 ">
                    <img src="../images/time.jpg" width="100%" height="150px">
                    <h5 class="mt-3">Saves your time</h5>
                </div> 
            
            </div>
        
        
        <?php include "footer.php"; ?>
        </div>
    </body>
    <script src="../js/formscript.js"></script>
</html>