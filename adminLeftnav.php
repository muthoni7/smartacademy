
<html>
<head>
        <meta http-equiv="refresh" content="300;url=logout.php" />
        
        <link rel="stylesheet" href="../css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/navstyle.css">
        <link rel="stylesheet" href="../css/fontawesome-free/css/all.css">
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../css/bootstrap4/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

            });
        </script>
        
</head>
<body>
        <?php include "adminav.php" ?> 
        <div class="wrapper1 ">
            <div class="row mt-0 pb-5 mr-0">
                    <!-- Sidebar -->
                        <nav id="sidebar" class="col-md-3">
                            
                    
                            <ul class="list-unstyled components">
                                
                                <li class="nav-item">
                                    <ul>
                                        <li><a href="new.php?label=otherAdmin">Other System Admins</a></li>
                                        <li><a href="new.php?label=ad-user">~All System Users</a></li>
                                    </ul>
                                </li>
                                
                                <li>
                                    <ul>
                                        <li class="nav-item"><a href="new.php?label=past">Units Done Before</a></li>
                                        <li class="nav-item"><a href="new.php?label=trends">How Students Have been Performing</a></li>
                                        <li><a href="new.php?label=u-units">~Unallocated Units</a></li>
                                        <li><a href="new.php?label=a-units">~Allocated Units</a></li>
                                        <li><a href="new.php?label=newunits">~Remove/Add Units</a></li> 
                                    </ul>
                                </li>
                        
                                <li>
                                    <ul>
                                        <li ><a href="new.php?label=feedback">~View-Feedback</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav> 
                    <div class="col-md-9 ml-auto mr-auto pl-0 pl-0">
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <button type="button" id="sidebarCollapse" class="btn btn-info">
                                        <i class="fas fa-align-left"></i>
                                    
                                </button>
                            </div>
                        </div>
                                <div class="container-fluid">
                    
        
               