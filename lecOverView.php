<?php 
session_start();
$idunit5 = $_SESSION["idunit5"];
$_SESSION["idunit5"] = $idunit5;

$idunit = $_SESSION["idunit5"];
$_SESSION["idunit"] = $idunit5;

$code = $_SESSION["code"];
$unitName = $_SESSION["unitName"];
$year = $_SESSION["year"];
$semester = $_SESSION["semester"];
$instructor = $_SESSION["instructor"];
?>
<style>  
    p{
        color:black!important;
    }
</style>
<div id="pastpage2" >
        <p5><?php echo $idunit5 ?></p5>
        <p class="mb-0"><?php echo $code." : ".$unitName."----"." YEAR  ".$year." : SEMESTER".$semester ?></p>
        <p>Instructor : <?php echo $instructor ?></p>
   
    <hr>
    <div class="row mb-2 ml-2">
        <div class="col-md-4 pl-1 pt-3 pb-3 pr-1 bg-dark mr-auto ml-auto">
            <button class="btn btn-secondary mb-1" id="lecdocs" style="float:left">
                Uploaded Documents
            </button> 

            <button class="btn btn-info mb-1" id="lecEnroll" style="float:left">
                Enrolled Students
            </button>

            <button class="btn btn-secondary mb-1" id="lecperform" style="float:left">
                Individual Students' Performance
            </button> 
            
            <button class="btn btn-info mb-1" id="lecgeneral" style="float:left">
                General Students Performance
            </button>

            <button class="btn btn-secondary mb-1" id="lecbtnFeedback" style="float:left">
                Course Feedback
            </button>
        </div>
        <div class="col-md-8 bg-light pt-2 ">
            <div id="lecfile">
                <?php 

                $idunit5 = $_SESSION["idunit5"];
                $page_rows = 5; 
                $query=mysqli_query($connection,"select count(id) from asynos where unitid='$idunit5'");
                include "pages.php";
                $nquery=mysqli_query($connection,"SELECT * FROM asynos where unitid='$idunit5'   $limit");
                ?> 
                <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
            
                    <th>Title </th>
                    <th>File Name</th>
                    <th>File Size(KB)</th>
                    <th>View</th>
                    


                    <tr><?php echo $paginationCtrls; ?></tr>
                        <?php

                        $bg = 0;
                        while($crow = mysqli_fetch_array($nquery)){
                            if ( $bg%2 == 0){
                                $class="even";
                            }else{
                                $class="light"; 
                            }
                            $bg++;
                            
                        ?>
                    <tr class="<?php echo $class; ?>">
                            <td><?php echo $crow['title'] ?></td>
                            <td><?php echo $crow['file'] ?></td>
                            <td><?php echo $crow['size'] ?></td>
                            <td><a href="viewfile.php?id=<?php echo $crow['id'] ?>" target="_blank">view file</a></td>
                    </tr>
                    <?php
                            }
                            ?>
                </table>

            </div>
            <div id="enrolledStudents">
                <?php
                    $idunit5 = $_SESSION["idunit5"];
                    $page_rows = 5; 
                    $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit5' ");
                    include "pages.php";
                    $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit5'   $limit");
                ?>
                <div class="ml-5 mr-5">
                    <table border="1" cellspacing="0" cellpadding="5" width="50%" align="center">
                        <th colspan="6"><p5>Enrolled STUDENTS</p5> </th>
                        
                        <tr><?php echo $paginationCtrls; ?></tr>
                                <?php
                                $bg = 0;
                                while($crow = mysqli_fetch_array($nquery)){
                                    if ( $bg%2 == 0){
                                        $class="light";
                                    }else{
                                        $class="even"; 
                                    }
                                    $bg++;
                                    
                                ?>
                        <tr class="<?php echo $class; ?>">
                        
                            <td> <?php echo $crow['username'] ?></td>

                        </tr>
                                    <?php
                            }
                        ?>
                        <tr><td colspan="3">Total No Of Enrolled Students: <?php echo $rows = $row[0]; ?></td></tr>
                    </table>
                </div>
            </div>
            <div id="lecEachStdn">
                <?php
                $unitt = $_SESSION["unitName"];    
                $idunit5 = $_SESSION["idunit5"];
                $page_rows = 5; 
                $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit5' ");
                include "pages.php";
                $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit5'   $limit");

                include "sum.php";
                ?>
                <table border="1" cellspacing="1" cellpadding="1" width="80%" align="center">
                    <th colspan="6"><p5>PERFORMANCE</p5></th>
                    <tr><th><p6>Username</p6></th><th><p6>Cat</p6></th><th><p6>Assignment</p6></th><th><p6>Main Exam</p6></th><th><p6>Total</p6></th><th><p6>Grades</p6></th></tr>
                    <tr><?php echo $paginationCtrls; ?></tr>
                            <?php
                               $bg = 0;
                               while($crow = mysqli_fetch_array($nquery)){
                                   if ( $bg%2 == 0){
                                       $class="light";
                                   }else{
                                       $class="even"; 
                                   }
                                   $bg++;
                                   
                               ?>
                   <tr class="<?php echo $class; ?>">

                        <td><?php echo $crow['username'] ?></td>
                        <td><label><?php echo $crow['cat'];?> </label></td>
                        <td><label><?php echo $crow['assyn'];?>   </label></td>
                        <td><label><?php echo $crow['mainexam'];?> </label></td>
                        <td><label><?php echo $crow['total'];?></label></td>
                        <td><label><?php include 'remarks.php' ?>  </label></td>
                    </tr>
                    <?PHP } ?>
                    <tr><td> <div id="tick">Average</div></td>
                        <td><label><?php echo $sum=$row['total'];?></label></td>
                        <td><label><?php echo $sum=$row1['total1'];?>  </label></td>
                        <td><label><?php echo $sum=$row2['total2'];?></label></td>
                        <td><label><?php echo $sum=$row3['total3'];?></label></td>
                        <td><label><?php echo $mygrade; ?></label></td>
                    </tr>
                </table>
            </div>
            <div id="lecAllPerform">

                <?php
                $query=("SELECT * FROM grades WHERE  unitid= '$idunit5' ")or die(mysqli_connect_error()); 
                $result=mysqli_query($connection,$query);
                if( mysqli_num_rows($result) > 1) {

                '<br>';
                // $_SESSION["idunit5"] = $idunit;
                $_SESSION["idunit"] = $idunit5;
                include 'lecform.html';

                }else{

                echo "NOTHING TO SHOW";

                } ?>
                <br><br>
            </div>
            <div id="lecfeedbk">
                <?php 
                $query=("SELECT * FROM lecturers WHERE unitid = '$idunit5'")or die (mysqli_connect_error()); 
                $result=mysqli_query($connection,$query);
                $row=mysqli_fetch_array($result);
                if( mysqli_num_rows($result) > 0) {
                ?>
                <div class="row">
                    <div class="col-md-8">
                        <?php
                        // $idunit5 = $_SESSION["idunit"];
                        $_SESSION["idunit"] = $idunit5;
                        include "lecextract.html";
                        '<br>';
                        ?>
                    </div>
                    <div class="col-md-4 ml-5">
                        <?php
                        include 'table.html';
                        ?><br>
                    </div>
                    
                    <br>
                </div>
                <?php

                }else{

                echo "nothing to show" ;  

                } ?>
                <br>
            </div>
        </div>
    </div>
</div> 
