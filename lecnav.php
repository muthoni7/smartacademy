<html>

<nav class="navbar navbar-light bg-primary navbar-expand-lg fixed-top">
            <a class="navbar-brand">
                <h2 style="color:white">
                    <img src="images/logo.jpg"  style="width:45px;height:45px;">
                        SmartAcademy
                </h2>
            </a>

            <div class="myprofile">
                <?php 
                    $query="select * from rjstrdb where username='$username'";
                    $result=mysqli_query($connection,$query);
                    $row=mysqli_fetch_array($result);
                if( $row['image'] != null){?>
                    <img src="../images/<?php echo($row['image']);?>" class="profile rounded-circle">
                    
                <?php }else{ ?>
                <div class="profile">
                    <i class="profile fa fa-user rounded-circle"></i>
                </div>
                <?php } ?>
            </div>
            <button class="navbar-toggler"
            type="button" data-toggle="collapse"
            data-target="#navbarContent" 
            aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active mr-3 mt-2">
                        <h3><?php include "time.php"; ?></h3>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="lechome.php"><button class="nav-link btn btn-primary btn-md  mb-1" >
                            HOME
                            </button>
                        </a>
                    </li>
    
                    <li class="nav-item">
                        <a class="nav-link" href="lecnew.php?label=chat"><button class="nav-link btn btn-primary btn-md  mb-1">
                                CHATS
                            </button>
                        </a>
                    </li>
                    
                        <li class="nav-item">
                        <a class="nav-link" href="lecnew.php?label=lecFeedback"><button class="nav-link btn btn-primary btn-md  mb-1">
                            FEEDBACK
                            </button>
                        </a>
                    </li>
                    
                    
                        

                    <li class="nav-item">
                        <a class="nav-link" href="lecnew.php?label=lecprofile"><button class="nav-link btn btn-primary btn-md  mb-1" >
                            PROFILE
                        </button>
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="lecnew.php?label=logout"><button class="nav-link btn btn-primary btn-md  mb-1">
                            LOGOUT
                            </button>
                        </a>
                    </li>
                    
                </ul>
            </div>
        </nav>
</html>