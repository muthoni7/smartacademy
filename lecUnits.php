

<html>
    <head>
        <style>
        #viewfile,#enrollStnd,#eachStdn,#allPerform,#feedbk,#timeddocs{
            display: none;
            font-size: 12px;
            margin-top: 5px
        }
        p{
            color:black!important;
        }   
        </style>
    </head>
    <body>
        <?php include "leftnav.php" ;
        
        $idunit = $_SESSION["idunit"];
        $code = $_SESSION["code"];
        $unitName = $_SESSION["unitName"];
        $year = $_SESSION["year"];
        $semester = $_SESSION["semester"];
        // $instructor = $_SESSION["instructor"];
        ?>
        <div class="container-fluid">
            
            <div class="row  ">
                <div class="col-md-12  justify-content-left" id="section2">
                    <section>
                        <div id="lechomepg">
                            <p5><?php echo $idunit ?></p5>
                            <p class="mb-0"><?php echo $code." : ".$unitName; ?></p>
                            <hr>
                            <div class="row mb-2 ml-2">
                                <div class="col-md-4 pl-1 pt-1 pb-3 pr-1 bg-dark mr-auto ml-auto">

                                    <button class="btn btn-secondary mb-1" id="docstimed" style="float:left">
                                        Timed Documents
                                    </button>

                                    <button class="btn btn-info mb-1" id="docs" style="float:left">
                                        Un-Timed Documents
                                    </button>

                                    <button class="btn btn-secondary mb-1" id="enrollStn" style="float:left">
                                        Enrolled Students
                                    </button> 

                                    <button class="btn btn-info mb-1" id="perform" style="float:left">
                                        Students' Performance
                                    </button>

                                    <button class="btn btn-secondary mb-1" id="general" style="float:left">
                                        General Performance
                                    </button> 

                                    <button class="btn btn-info mb-1" id="btnFeedback" style="float:left">
                                        Course Feedback
                                    </button>

                                    <a href="lecnew.php?label=end"><button class="btn btn-secondary" id="btnFeedback" style="float:left">
                                        MARK THE UNIT AS ENDED
                                        </button>
                                    </a>

                                </div>
                                <div class="col-md-8 bg-light ">
                            
                                    <div id="timeddocs"><?php

                                        include "view2.php";
                                        include("databasecon.php");
                                        SESSION_START();
                                        $username=$_SESSION["username"];
                                        $unitid = $_SESSION["idunit"];

                                        
                                        ?>

                                        <form action="fileupload2.php"  method="post" enctype="multipart/form-data" class="mt-1"><hr>
                                            <style>
                                                label{
                                                    font-size:15px
                                                }
                                            </style>
                                            <div class="row">
                                                <div class="col-md-6 mb-3">
                                                    <label>Title</label> <input type="text" name="title" maxlength="20" /required><br>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Uploaded Date:</label> <?php date_default_timezone_set("Africa/Nairobi"); echo date("Y/m/d") ?>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type = "file" class="btn btn-info" name="file"/>
                                                    <label>only pdf and .docx files are allowed:<br></label>
                                                </div>
                                            
                                                <div class="col-md-6">
                                                    <input type="hidden" name="uploaddate" value="<?php date_default_timezone_set("Africa/Nairobi"); echo date("Y/m/d h:i:sa") ?>"  /required><br>
                                                    <label for="sdate">Submition Date</label> <input type="date" name="sdate"  /required><br><br>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-md-4 ml-auto mr-auto" style="font-size: 22px;">
                                                    <input type="submit" class="btn-block btn-success" value="Upload" name="submit1">
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="viewfile">
                                        <?php 
                                    
                                        $idunit = $_SESSION["idunit"];
                                        $page_rows = 5; 
                                        $query=mysqli_query($connection,"select count(id) from asynos where unitid='$idunit' and enddate ='' $limit");
                                        include "pages.php";
                                        $nquery=mysqli_query($connection,"SELECT * FROM asynos where unitid='$idunit' and enddate = ''  $limit");
                                        ?>

                                        <table width="100%" border="1">
                                            <tr>
                                                <th>Title</th>
                                                <th>File Name</th>
                                                <th>File Size(KB)</th>
                                                <th>View File</th>
                                                <th>Delete File</th>
                                            </tr>


                                            <tr><?php echo $paginationCtrls; ?></tr>
                                            <?php
                                            
                                                while($crow = mysqli_fetch_array($nquery)){
                                                    
                                            ?>
                                            <tr >
                                                <td><?php echo $crow['title'] ?></td>
                                                <td><?php echo $crow['file'] ?></td>
                                                <td><?php echo $crow['size'] ?></td>
                                                <td class="text-info"><a target="_blank" href="viewfile.php?id=<?php echo $crow['id'] ?>">view</a></td>
                                                <td class="text-danger"><a href="deletefile.php?id=<?php echo $crow['id']?>">Delete</a></td>
                                            </tr>
                                                    <?php
                                                    }
                                                    ?>
                                        </table>

                                        <?php
                                        if(isset($_POST['upload']))
                                        {    
                                            $idunit = $_SESSION["idunit"];        
                                            $title=$_POST['title'];

                                            $file = rand(1000,100000)."-".$_FILES['file']['name'];
                                            $imgdata = addslashes(file_get_contents($_FILES['file']['tmp_name']));
                                            $file_size = $_FILES['file']['size'];
                                            $file_type = $_FILES['file']['type'];

                                            $sql=("SELECT * FROM asynos WHERE title = '$title'");
                                            $rst= mysqli_query($connection,$sql);
                                            if(!$rst || mysqli_num_rows($rst1) <= 0) {

                                                $sql="INSERT INTO asynos(unitid,username,file,type,size,yeart,unitt,img,uploaddate,enddate,title) VALUES('$idunit','$username','$file','$file_type','$file_size','$yeart','$unitt','$imgdata','','','$title')";
                                                $result= mysqli_query($connection,$sql); 
                                                if(!$result) 
                                                {
                                                    echo "<script language='JavaScript'>
                                                    alert('	an error occured')
                                                    window.location.href='lecUnits.php';
                                                    </script>";
                                                }else
                                                {
                                                    echo "<script language='JavaScript'>
                                                    alert('success')
                                                    window.location.href='lecUnits.php';
                                                    </script>";
                                                }
                                            }
                                            else
                                            {
                                                echo "<script language='JavaScript'>
                                                alert('TITLE ALREADY EXITS. PLEASE USE A DIFFERENT TITLE')
                                                window.location.href='fileupload.php';
                                                </script>";
                                            }
                                        }
                                        ?>
                                        <br>
                                        <form action="" method="post" enctype="multipart/form-data" class="mt-3"><hr>
                                            
                                            <div class="row">
                                                <div class="col-md-4 mr-0">
                                                    <h3>Upload a File </h><br> 
                                                </div>
                                                <div class="col-md-6 ml-0">
                                                Title:-<input class="form-control" type="text" name="title" maxlength="20" /required><br>
                                                </div>
                                            </div>

                                            <h6>select file to upload (only pdf and .docx files are allowed):
                                            <input type = "file" name="file" class="btn btn-info" required/>
                                            <input type="submit" class="btn btn-success" value="upload" name="upload"></h6>
                                        </form>
                                    </div>
                             
                                    <div id="enrollStnd">
                                        <?php
                                        $idunit = $_SESSION["idunit"];
                                        $page_rows = 5; 
                                        $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' ");
                                        include "pages.php";
                                        $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit'   $limit");
                                        ?>

                                        <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                            <th colspan="6"><h4>Enrolled Students</h4> </th>
                                            
                                            <tr><?php echo $paginationCtrls; ?></tr>
                                            
                                                    <?php
                                                        while($crow = mysqli_fetch_array($nquery)){
                                                        ?>
                                            <tr>
                                                
                                                <td> <?php echo $crow['username'] ?></td>

                                            </tr>
                                                <?php
                                                }
                                                ?>
                                            <tr><td colspan="3"><p4>TOTAL NO OF ENROLLED STUDENTS: <?php echo $rows = $row[0]; ?></p4></td></tr>
                                        </table>
                                    </div>
                                        
                                    <div id="eachStdn">
                                        <?php
                                        $unitt = $_SESSION["unitName"];

                                        if(isset($_POST['submitfm'])){
                                            $unitt = $_SESSION["unitName"];
                                            for($i=0; $i<count($_POST["id"]); $i++){
                                                $cat= $_POST["cat"][$i];
                                                $assyn= $_POST["assyn"][$i];
                                                $mainexam= $_POST["mainexam"][$i];
                                                $total= $_POST["cat"][$i]+$_POST["assyn"][$i]+$_POST["mainexam"][$i];
                                                $grades= $_POST["grades"][$i];



                                                if ($total >= 70)
                                                {
                                                    $grades="A";
                                                    //echo 'A';

                                                }else if ($total >= 60)
                                                {
                                                    $grades="B";
                                                    //echo 'B';
                                                }else if ($total >= 50)
                                                {
                                                    $grades="C";
                                                    //echo 'C';
                                                }else if ($total >= 40)
                                                {
                                                    $grades="D";
                                                    //echo 'D';
                                                }else
                                                {
                                                    $grades='F';
                                                    //echo "F";
                                                }

                                                $query = "UPDATE enroll SET cat='$cat' WHERE id='" . $_POST["id"][$i] . "'";
                                                $result = mysqli_query($connection,$query);

                                                $query = "UPDATE enroll SET assyn='$assyn' WHERE id='" . $_POST["id"][$i] . "'";
                                                $result = mysqli_query($connection,$query);

                                                $query = "UPDATE enroll SET mainexam='$mainexam' WHERE id='" . $_POST["id"][$i] . "'";
                                                $result = mysqli_query($connection,$query);

                                                $query = "UPDATE enroll SET lecturer='$username' WHERE id='" . $_POST["id"][$i] . "'";
                                                $result = mysqli_query($connection,$query);

                                                $query = "UPDATE enroll SET total='$total' WHERE id='" . $_POST["id"][$i] . "'";
                                                $result = mysqli_query($connection,$query);

                                                $query = "UPDATE enroll SET grades='$grades' WHERE id='" . $_POST["id"][$i] . "'";
                                                $result = mysqli_query($connection,$query);

                                            }
                                            $query11=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' and grades='A' ");
                                            $row11 = mysqli_fetch_row($query11);	
                                            $rows11 = $row11[0];

                                            $query12=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' and grades='B' ");
                                            $row12 = mysqli_fetch_row($query12);	
                                            $rows12 = $row12[0];

                                            $query13=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' and grades='C' ");
                                            $row13 = mysqli_fetch_row($query13);	
                                            $rows13 = $row13[0];

                                            $query14=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' and grades='D' ");
                                            $row14 = mysqli_fetch_row($query14);	
                                            $rows14 = $row14[0];

                                            $query15=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' and grades='F' ");
                                            $row15 = mysqli_fetch_row($query15);	
                                            $rows15 = $row15[0];


                                            $query=("SELECT * FROM grades WHERE lecreg = '$username' AND unitid= '$idunit' ")or die(mysqli_connect_error()); 
                                            $result=mysqli_query($connection,$query);
                                            if( mysqli_num_rows($result) > 1) {
                                               
                                                $result=mysqli_query($connection,"UPDATE grades SET number='$rows11' WHERE grade='A' AND unitid='$idunit' AND lecreg = '$username'");
                                                $result=mysqli_query($connection,"UPDATE grades SET number='$rows12' WHERE grade='B' AND unitid='$idunit' AND lecreg = '$username'");
                                                $result=mysqli_query($connection,"UPDATE grades SET number='$rows13' WHERE grade='C' AND unitid='$idunit' AND lecreg = '$username'");
                                                $result=mysqli_query($connection,"UPDATE grades SET number='$rows14' WHERE grade='D' AND unitid='$idunit' AND lecreg = '$username'");
                                                $result=mysqli_query($connection,"UPDATE grades SET number='$rows15' WHERE grade='F' AND unitid='$idunit' AND lecreg = '$username'");

                                            }else{
                                                $query = "INSERT INTO grades(unitid,unitname,lecreg,grade,number,yeart) VALUES ('$idunit','$unitt','$username','A','$rows11','')";
                                                $result=mysqli_query($connection,$query);

                                                $query = "INSERT INTO grades(unitid,unitname,lecreg,grade,number,yeart) VALUES ('$idunit','$unitt','$username','B','$rows12','')";
                                                $result=mysqli_query($connection,$query);
                                                $query = "INSERT INTO grades(unitid,unitname,lecreg,grade,number,yeart) VALUES ('$idunit','$unitt','$username','C','$rows13','')";
                                                $result=mysqli_query($connection,$query);
                                                $query = "INSERT INTO grades(unitid,unitname,lecreg,grade,number,yeart) VALUES ('$idunit','$unitt','$username','D','$rows14','')";
                                                $result=mysqli_query($connection,$query);
                                                $query = "INSERT INTO grades(unitid,unitname,lecreg,grade,number,yeart) VALUES ('$idunit','$unitt','$username','F','$rows15','')";
                                                $result=mysqli_query($connection,$query);
                                                if ($result){
                                                    echo "<script language='JavaScript'>
                                                    alert('Data well submitted')
                                                    window.location.href='lecUnits.php';
                                                    </script>";
                                                }
                                            }
                                        
                                        }
                                                
                                        $idunit = $_SESSION["idunit"];
                                        $page_rows = 5; 
                                        $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' ");
                                        include "pages.php";
                                        $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit'   $limit");
                                        ?>
                                        
                                        
                                        <form method="POST" >
                                            <?php
                                            include "sum.php";
                                            ?>
                                            <table border="1" cellspacing="1" cellpadding="1" width="100%" align="center">
                                                <th colspan="6"><h6>Student's Performance </h6></th>
                                                <tr><th><p6>Username</p6></th><th><p6>Cat</p6></th><th><p6>Assignment</p6></th><th><p6>Main Exam</p6></th><th><p6>Total</p6></th><th><p6>Grades</p6></th></tr>
                                                <tr><?php echo $paginationCtrls; ?></tr>
                                                    <?php
                                                        while($crow = mysqli_fetch_array($nquery)){
                                                    ?>
                                                <tr>

                                                    <td> <?php echo $crow['username'] ?></td>
                                                    <input type="hidden" name="id[]" value=<?php echo $crow['id'];?> />
                                                    <td><input style="width:5em" type="number" max="20" name="cat[]" value=<?php echo $crow['cat'];?>   /required></td>
                                                    <td><input style="width:5em" type="number" max="10" name="assyn[]" value=<?php echo $crow['assyn'];?>   /required></td>
                                                    <td><input style="width:5em" type="number" max="70" name="mainexam[]" value=<?php echo $crow['mainexam'];?> /required></td>
                                                    <td><input style="width:5em" readonly type="number" name="total[]" value=<?php echo $crow['total'];?>   /required></td>
                                                    <td><input style="width:5em" readonly type="text" name="grades[]"  value=<?php include 'remarks.php' ?>  /required></td>
                                                </tr>
                                                <?php
                                                }//<tr  "style=align:right"><td colspan="3">TOTAL NO OF ENROLLED STUDENTS: <?php echo $rows = $row[0]; //</td></tr>
                                                ?>
                                                
                                                <tr><td> <div id="tick">AVERAGE</div></td>
                                                    <td><input readonly type="float" style="width:5em" name="acat" value="<?php echo round($sum=$row['total'],1);?>"  /required></td>
                                                    <td><input readonly type="float" style="width:5em" name="aassyn" value="<?php echo round($sum=$row1['total1'],1);?>"  /required></td>
                                                    <td><input readonly type="float" style="width:5em" name="amain" value="<?php echo round($sum=$row2['total2'],1);?>"  /required></td>
                                                    <td><input readonly type="float" style="width:5em" name="atotal" value="<?php echo round($sum=$row3['total3'],1);?>"  /required></td>
                                                    <td><input readonly type="float" style="width:5em" name="agrades" value="<?php echo $mygrade; ?>"/required></td>
                                                </tr>

                                                <tr><td COLSPAN="5"><button  class="btn btn-success btn-md" name="submitfm" >SUBMIT</button></td></tr>
                                            </table>
                                        </form>
                                    </div>
                                    <div id="allPerform">
                                    
                                        <?php
                                        $query=("SELECT * FROM grades WHERE  unitid= '$idunit' ")or die(mysqli_connect_error()); 
                                        $result=mysqli_query($connection,$query);
                                        if( mysqli_num_rows($result) >= 1) {
                                        echo $idunit = $_SESSION["idunit"];
                                            include 'form.html';

                                        }else{
                                            echo  "PLEASE FILL STUDENT'S MARKS FIRST";
                                            
                                        } ?>
                                
                                    </div>
                                    <div id="feedbk">
                                    
                                        <?php 
                            
                                        $query=("SELECT * FROM lecturers WHERE unitid = '$idunit'")or die (mysqli_connect_error()); 
                                        $result=mysqli_query($connection,$query);
                                        $row=mysqli_fetch_array($result);
                                        if( mysqli_num_rows($result) >=1 ) {
                                            include "extract.html";?><br><?php
                                            include 'table.html';
                                        }else{

                                            ?>
                                            <form method="POST">
                                                <style>
                                                    p6{
                                                        font-size:12px;
                                                    }
                                                </style>
                                                
                                                <p>For each item, check the box that best describes how the unit was delivered.</p>

                                                <table border="1" cellspacing="0" cellpadding="5" width="80%" >
                                                    <tr><th><p6>ABOUT THE UNITS</p6></th><th><p6>1.Unsatisfactory</p6></th><th><p6>2.Needs Improvement</p6></th><th><p6>3.Satisfactory</p6></th><th><p6>4.Outstanding</p6></th><th><p6> N/O.Not-Observed</p6></th></tr>
                                                    <tr><td><p3>students attendance in class</p3></td><td><input type="radio" name="sattendance" value="1" required></td>
                                                        <td><input type="radio" name="sattendance" value="2"/></td>
                                                        <td><input type="radio" name="sattendance" value="3"></td>
                                                        <td><input type="radio" name="sattendance" value="4"></td>
                                                        <td><input type="radio" name="sattendance" value="0"></td>
                                                    </tr>
                                                    <tr><td><p3>level of student cooperation in class</p3></td><td><input type="radio" name="stcpration" value="1" required></td>
                                                        <td><input type="radio" name="stcpration" value="2"></td>
                                                        <td><input type="radio" name="stcpration" value="3"></td>
                                                        <td><input type="radio" name="stcpration" value="4"></td>
                                                        <td><input type="radio" name="stcpration" value="0"></td>
                                                    </tr>
                                                    <tr><td><p3>students puntuality</p3></td><td><input type="radio" name="spuntuality" value="1" required></td>
                                                        <td><input type="radio" name="spuntuality" value="2"></td>
                                                        <td><input type="radio" name="spuntuality" value="3"></td>
                                                        <td><input type="radio" name="spuntuality" value="4"></td>
                                                        <td><input type="radio" name="spuntuality" value="0"></td>
                                                    </tr>
                                                    <tr><td><p3>Availability of study materials</p3></td><td><input type="radio" name="amaterials" value="1" required></td>
                                                        <td><input type="radio" name="amaterials" value="2"></td>
                                                        <td><input type="radio" name="amaterials" value="3"></td>
                                                        <td><input type="radio" name="amaterials" value="4"></td>
                                                        <td><input type="radio" name="amaterials" value="0"></td>
                                                    </tr>
                                                    <tr><td><p3>Usefulness of tests and exams</p3></td><td><input type="radio" name="utests" value="1" required></td>
                                                        <td><input type="radio" name="utests" value="2"></td>
                                                        <td><input type="radio" name="utests" value="3"></td>
                                                        <td><input type="radio" name="utests" value="4"></td>
                                                        <td><input type="radio" name="utests" value="0"></td>
                                                    </tr>
                                                    <tr><td><p3>How the students benefited from the unit</p3></td><td><input type="radio" name="sbenefit" value="1" required></td>
                                                        <td><input type="radio" name="sbenefit" value="2"></td>
                                                        <td><input type="radio" name="sbenefit" value="3"></td>
                                                        <td><input type="radio" name="sbenefit" value="4"></td>
                                                        <td><input type="radio" name="sbenefit" value="0"></td>
                                                    </tr>
                                                </table>
                                                <input id="button" class="btn btn-danger" type="reset" name="cancel" value="cancel">
                                                <input id="button" class="btn btn-primary" type="submit" name="submit" value="submit">
                                                

                                            </form>
                                            <?php  
                                            
                                                if (isset($_POST['submit']))
                                            {
                                        
                                            
                                                $unitt = $_SESSION["code"];
                                                $yeart = $_SESSION["year"];
                                                $unitid = $_SESSION["idunit"];
                                                
                                                $sattendance = $_POST['sattendance'];
                                                $stcpration = $_POST['stcpration'];
                                                $spuntuality = $_POST['spuntuality'];
                                                $amaterials = $_POST['amaterials'];
                                                $utests = $_POST['utests'];
                                                $sbenefit = $_POST['sbenefit'];



                                                $query = "INSERT INTO lecturers(unitid,unitt,styear,semester,lecturername,student_attendance,student_cooperation,student_puntuality,availability_of_study_materials,usefulness_of_tests,unit_benefit) 
                                                                VALUES ('$unitid','$unitt','$yeart','$semester','$username','$sattendance','$stcpration','$spuntuality','$amaterials','$utests','$sbenefit')";
                                                $result=mysqli_query($connection,$query);

                                                if ($result){
                                                    $query1 = "INSERT INTO data(unitid,username,unitt,yeart,field,details) VALUES ('$unitid','$username','$unitt','$yeart','student_attendance','$sattendance')";
                                                    $result1=mysqli_query($connection,$query1);

                                                    if ($result1){
                                                        $query2 = "INSERT INTO data(unitid,username,unitt,yeart,field,details) VALUES ('$unitid','$username','$unitt','$yeart','student_cooperation','$stcpration')";
                                                        $result2=mysqli_query($connection,$query2);

                                                        if ($result2){
                                                            $query3 = "INSERT INTO data(unitid,username,unitt,yeart,field,details) VALUES ('$unitid','$username','$unitt','$yeart','student_puntuality','$spuntuality')";
                                                            $result3=mysqli_query($connection,$query3);

                                                            if ($result3){
                                                                $query4 = "INSERT INTO data(unitid,username,unitt,yeart,field,details) VALUES ('$unitid','$username','$unitt','$yeart','availability_of_study_materials','$amaterials')";
                                                                $result4=mysqli_query($connection,$query4);

                                                                if ($result4){
                                                                    $query5 = "INSERT INTO data(unitid,username,unitt,yeart,field,details) VALUES ('$unitid','$username','$unitt','$yeart','usefulness_of_tests','$utests')";
                                                                    $result5=mysqli_query($connection,$query5);

                                                                    if ($result5){
                                                                        $query6 = "INSERT INTO data(unitid,username,unitt,yeart,field,details) VALUES ('$unitid','$username','$unitt','$yeart','benefit earned to students','$sbenefit')";
                                                                        $result6=mysqli_query($connection,$query6);

                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if(!$result6) 
                                                {

                                                    echo "<script language='JavaScript'>
                                                    alert('AN ERROR OCCURRED!!')
                                                    window.location.href='lecUnits.php';
                                                    </script>";
                                                }else
                                                {
                                                    echo "<script language='JavaScript'>
                                                    alert('DETAILS HAVE BEEN WELL SUBMITTED')
                                                    window.location.href='lecUnits.php';
                                                    </script>";

                                                }

                                            }
                                        }
                                        ?>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div> 

            </div>
            </nav>
        </div>
        </div>
        </div>
        </div>
        <?php include "footer.php"; ?>
            
        </div>
    </body>
    <script src="../js/formscript.js"></script>
</html>