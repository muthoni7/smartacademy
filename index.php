
<html>
    <head>
        <title>SmartAcademy</title>
        
        <link rel="stylesheet" href="../css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/fontawesome-free/css/all.css">
        <link rel="stylesheet" type="text/css" href="css/animate/animate.min.css"> 
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="css/bootstrap4/js/bootstrap.min.js"></script>
        <script src="js/sweetalert.min.js"></script> 
        
        <style>
            #loginpg{
                display: none;
            }
            #aboutpg{
                display: none;
            }
                #registerpg{
                display: none;
            }
            h4{
                color:saddlebrown;
                text-shadow: 2px 0 0 white;
                font-weight: bold;
            }
            label{
                color: peru;
                
                font-size: 15px;
            }
            .text-danger{
                font-size:15px;
            }
        </style>
    </head>
    <body>
        <?php
            error_reporting(0);
            session_start();
            echo $msghere = $_SESSION["msgHere"];
                if ($msghere === 'out'){
                    echo"<script>
                            $(document).ready(function() {
                                swal('You Have Been Auto-Logged Out',
                                'Please Login Again TO Enjoy Our Services','error');
                            });
                            </script>";
                }
                $_SESSION ['msgHere'] = '';
           
            
        ?>
    
           
    <nav class="navbar bg-primary navbar-expand-lg fixed-top">
        <a class="navbar-brand"><h2 style="color:white">
        <img src="images/logo.jpg"  style="width:45px;height:45px;">
            SmartAcademy
            </h2>
        </a>
        <button class="navbar-toggler"
        type="button" data-toggle="collapse"
        data-target="#navbarContent" 
        aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item  mr-3 mt-2">
                    <h3><?php include "time.php"; ?></h3>
                </li>
                <li class="nav-item  mr-5">
                    <button class="nav-link btn btn-secondary btn-block  mb-1" id="home">
                        Home
                    </button>
                </li>
                <li class="nav-item  mr-5">
                    <button class="nav-link btn btn-secondary  btn-block   mb-1" id="login">
                        Login
                    </button>
                </li>
                <li class="nav-item  mr-5 ">
                    <button class="nav-link btn btn-secondary btn-block " id="register">
                        Register
                    </button>
                </li>
                <li class="nav-item  mr-5 ">
                    <button class="nav-link btn btn-secondary btn-block " id="about">
                        About us
                    </button>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row  mb-0 justify-content-center " style="margin-top: 60px;">     
        <div class="col-md-11 fullimage " id="section">
            <div id="homepg" class="mt-0 col-md-12 pt-0 pl-0 " style="color: black;">
                <div class="row mr-auto ml-auto"> 
                    <div class=" thispage col-md-4  mt-2 ml-0"><?php include "slide.html";?></div>
                    <div class="col-md-8 mt-5 ">
                        Let's Learn With Ease
                    
                        <div class="col-md-12 mt-5" style="font-size:14;font-family:Times New Roman">
                            <br>
                            SmartAcademy is a e-learning system that is meant to make education more efficient.
                             Its not meant to replace normal class attendance, instead its meant to make them better, more efficient and time saving.
                            <br>
                            We believe that a better tomorrow can be made by identifying the wrongs done yesterday, planning on how to rectify them today and carrying out our plan tomorrow.
                            
                        </div>
                    </div>
                </div>
                
            </div>
            <div  class=" row justify-content-center mb-5">
            <style>
            label{
                color:green;
            }
            </style>
                    <div id="registerpg" class="col-md-8 " >
                        <div class='accountError alert alert-danger text-center' role='alert'> ALL FIELDS ARE MADATORY</div>
                        <form id="regform" method="post">
                            <h4 class="ml-5">PLEASE FILL-IN YOUR PERSONAL DETAILS BELOW</h4>
                            
                            <div class="row">
                                <div class="col-md-6"> 
                                    <div class="form-group">
                                        <label for="profession">PROFESSION<span class="required">*</span></label>
                                        <select class="form-control " name="profession" id="profession">
                                            <option selected hidden value="">CHOOSE PROFESSION</option>
                                            <option value="student">STUDENT</option>
                                            <option value="lecturer">LECTURER</option>
                                        </select>
                                        <span class="text-danger professionerror"></span>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="form-group"> 
                                        <label for="fullname">FULLNAME <span class="required">*</span> </label>
                                        <input class="form-control " type="name" id="fullname" name="fullname" placeholder="fullname"/>
                                        <span class="text-danger fullnameerror"></span>
                                    </div>
                                </div>
                        
                                
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="myemail">Email <span class="required">*</span> </label>
                                    <input class="form-control " type="text" id="myemail" name="email" placeholder="example@email.com" />
                                    <span class="text-danger emailerror"></span>
                                    <span class="text-danger emailused"></span>
                                </div>
                                <div class="col-md-6"> 
                                    <label for="phoneno">PHONE NUMBER <span class="required">*</span> </label><br>
                                    <div class="row">
                                        <div class="col-md-3 ">
                                            <input class="form-control"  readonly value="+254" >
                                        </div>
                                        <div class="col-md-9 ">
                                            <input class="form-control" type="number" id="phoneno" name="phoneno" placeholder="7********" />
                                            <span class="msg">(only kenyan phone numbers)</span>
                                            <span class="text-danger phonenoerror"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mypassword">PASSWORD <span class="required">*</span> </label>
                                        <input class="form-control form-control-danger" type="password" id="password" name="password" placeholder="********"
                                            onblur="checkvalue(this)" />
                                            <span class="text-danger passworderror"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cmypassword">CONFIRM PASSWORD <span class="required">*</span></label>
                                        <input class="form-control form-control-danger" type="password" id="cpassword" name="cpass" placeholder="********"/>
                                        <br><span class="text-danger cpasserror"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-danger btn-md ml-0" type="reset" name="cancel">CANCEL</button>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-success btn-md " type="submit" name="submit"> SUBMIT</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div  class="mt-5 row justify-content-center">
                    <div id="loginpg" class="col-md-8 text-center" >
                        <div class='accountError alert alert-danger' role='alert'> ALL FIELDS ARE MADATORY</div>
                        <form action="" method="post" id="loginpage">
                            <h4>PLEASE LOGIN BELOW</h4>
                            <div class="row justify-content-center">
                                <div class="col-md-2"><label for="email">EMAIL </label></div>
                                    <div class="col-md-4">
                                        <input type="text" id="email" name="email" placeholder="example@gmail.com"  style="width:90%">
                                        <span class="required">*</span>
                                        <br><span class="text-danger emailerror"></span>
                                    </div>
                            </div>

                            <div class="row justify-content-center">
                                    <div class="col-md-2 mr-0"><label for="mypassword">PASSWORD </label></div>
                                    <div class="col-md-4 ml-0">
                                        <input type="password" id="mypassword" name="password" placeholder="*******" style="width:90%">
                                        <span class="required">*</span>
                                        <br><span class="text-danger passworderror"></span>
                                    </div>
                            </div>

                                <div class="row justify-content-center mt-1">
                                    <div class="col-md-4">
                                        <button class="btn btn-danger btn-md" type="reset" name="cancel">CANCEL</button>
                                    </div>
                                    <div class="col-md-4 text-center">
                                        <button class="btn btn-success btn-md" type="submit" name="submit">LOGIN</button>
                                    </div>
                                     
                                </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include "about.php"; ?>
        <?php include "footer.php"; ?>
    </div>
    </body>
    
    <script src="js/formscript.js"></script>
    <script src="js/formValidation.js"></script>
</html>