

<html>
    <head>
        <title>SmartAcademy</title>

        <link rel="stylesheet" href="../css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../css/bootstrap4/js/bootstrap.min.js"></script>

    </head>
    <body>
        <?php include "adminLeftnav.php"; ?>   
        <div class="container-fluid">
            
            <div class="row justify-content-center">
                <div class="col-md-12 ">
                    <div class="row ">
                        <div class="col-md-12">
                            <section>
                                <?php
                                $label = $_SESSION["label"];                        
                                if ($label == 'past') { 
                                    $page_rows = 5; 
                                    $query=mysqli_query($connection,"select count(id) from `units` WHERE status='ended' ");
                                    include "pages.php";
                                    $nquery=mysqli_query($connection,"select * from `units` WHERE status='ended'   $limit");
                                    
                                
                                    ?>


                                    <form METHOD="POST">
                                        <?php
                                        if(isset($_POST['submit'])){ 
                                            ?>

                                            <br>
                                            <td><input type="text" name="id"  maxlength="20" /required placeholder="enter unit's id"></td>
                                            <td><input type="submit" name="submit" value="SEARCH"/><br><br><br></td>

                                            <?php 
                                            $id=$_POST['id'];
                                            $query= ("SELECT *FROM `units` WHERE status='ended' and idunit='$id'");
                                            $result=mysqli_query($connection,$query);
                                            ?>

                                            <table border="1" align="center" width="100%">
                                                <th colspan="7"><p5>PAST UNITS</p5></th>
                                                <tr><th></th><th><p6>Unit's' ID</p6></th><th><p6>Year</p6></th><th><p6>Unit Code</p6></th><th><p6>Unit Name</p6></th></tr>

                                                <?php
                                                while($crow = mysqli_fetch_array($result))
                                                {
                                                    ?>
                                                <tr>
                                                    <td><div id="tick"> <i class="fa fa-thumbs-up"/> </div></td>
                                                    <td> <?php echo $crow['idunit'] ?></td>
                                                    <td> <?php echo $crow['year'];  ?></td>
                                                    <td> <?php echo $crow['unitcode'] ?></td>
                                                    <td> <?php echo $crow['unitname'] ?></td>

                                                    <td><a href="new.php? label=overView&&idunit=<?php echo $crow['idunit'];?>
                                                    &&code=<?php echo $crow["unitcode"];?>
                                                        &&unitName=<?php echo $crow["unitname"]; ?>
                                                        &&year= <?php echo $crow['year'];?>&&semester= <?php echo $crow['semester'];?>&&instructor=<?php echo $crow['LECTURER'];?>"><i class="fa fa-paper-plane"></i></a>
                                                    <br>
                                                    </td>
                                                    <td>
                                                        <div class="thisText" align="right">
                                                            <a href="new.php?unitid=<?php echo $crow["idunit"]; ?>&label=ended">REACTIVATE THE UNIT</a>
                                                        </div>
                                                    </td>                                                         
                                                        <?php } ?>
                                                </tr>
                                            </table>
                                            <?php
                                        } else {
                                            ?>

                                            <td><input type="text" name="id"  maxlength="20" /required placeholder="enter unit's id"></td>
                                            <td><input type="submit" name="submit" value="SEARCH"></td>

                                        

                                            <br>
                                            <table border="1" width="100%">
                                                <th colspan="7"><p5>PAST UNITS</p5></th>
                   
                                                <tr><th >Unit's ID</th><th>Lecturer</th><th>Year</th>
                                                <th>Unit Code</th><th>Unit Name</th><th>Have An Over-View</th><th></th></tr>

                                               <?php
                                                    $bg = 0;
                                                    while($row1 = mysqli_fetch_array($nquery)){
                                                        if ( $bg%2 == 0){
                                                            $class="light";
                                                        }else{
                                                            $class="even"; 
                                                        }
                                                        $bg++;

                                                ?>
                                                <tr class="<?php echo $class; ?>">
                                                    <td> <?php echo "&nbsp".$row1['idunit'] ?></td>
                                                    <td> <?php echo "&nbsp".$row1['LECTURER'] ?></td>
                                                    <td> <?php echo $row1['year']?></td>
                                                    <td> <?php echo $row1['unitcode'] ?></td>
                                                    <td> <?php echo $row1['unitname'] ?></td>
                                                    <td style="color:blue;font-size:20px;"><a href="new.php? label=overView&&idunit=<?php echo $row1['idunit'];?>
                                                    &&code=<?php echo $row1["unitcode"];?>
                                                        &&unitName=<?php echo $row1["unitname"]; ?>
                                                        &&year= <?php echo $row1['year'];?>&&semester= <?php echo $row1['semester'];?>&&instructor=<?php echo $row1['LECTURER'];?>">View</a>
                                                    <br>
                                                    </td>
                                                    <td>
                                                        <div id="tick" align="right">
                                                            <a href="new.php?unitid=<?php echo $row1["idunit"]; ?>&label=ended"><h6>Reactivate</h6></a>
                                                        </div><br>
                                                    </td>                                            
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </table>
                                            <?php
                                        } ?>
                                        <div id="pages"><?php echo $paginationCtrls; ?></div>
        
                                    </form>
                                    <?php 
                                } 

                                else if($label == 'overView'){
                                    include "overView.php";
                                }?>
                            </section>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php include "footer.php"; ?>
    </body>
    <script src="../js/formscript.js"></script>
</html>