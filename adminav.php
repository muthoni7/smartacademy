<?php
    //include "checkIn.php";
    include "checkcode.php";
?>
<nav class="navbar navbar-light bg-primary navbar-expand-lg fixed-top">
            <a class="navbar-brand">
                <h2 style="color:white">
                    <img src="images/logo.jpg"  style="width:45px;height:45px;">
                        SmartAcademy
                </h2>
            </a>

            <div class="myprofile">
                <?php 
                    $query="select * from rjstrdb where username='$username'";
                    $result=mysqli_query($connection,$query);
                    $row=mysqli_fetch_array($result);
                if( $row['image'] != null){?>
                    <img src="../images/<?php echo($row['image']);?>" class="profile rounded-circle">
                    
                <?php }else{ ?>
                    <div class="profile">
                        <i class="profile fa fa-user rounded-circle"></i>
                    </div>
                <?php } ?>
            </div>

            <button class="navbar-toggler"
            type="button" data-toggle="collapse"
            data-target="#navbarContent" 
            aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav ml-auto">
                    
                   
                    <li class="nav-item">
                        <li class="nav-item active mr-3 mt-2">
                            <h3><?php include "time.php"; ?></h3>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="admhom.php"><button class="nav-link btn btn-primary btn-md mr-3 mb-1">
                                HOME
                                </button>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="new.php?label=chat">
                                <button class="nav-link btn btn-primary btn-md  mb-1" >
                                CHAT
                                </button>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="new.php?label=profile">
                                <button class="nav-link btn btn-primary btn-md  mb-1" >
                                    VIEW PROFILE
                                </button>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="new.php?label=logout">
                                <button class="nav-link btn btn-primary btn-md  mb-1" >
                                    LOG OUT
                                </button>
                            </a>
                        </li>
                        
                    </li>  
                                          
                </ul>
            </div>
        </nav>