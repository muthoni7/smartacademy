<?php
$idunit = $_SESSION["idunit"];
$code = $_SESSION["code"];
$unitName = $_SESSION["unitName"];
$year = $_SESSION["year"];
$semester = $_SESSION["semester"];
$instructor = $_SESSION["instructor"];
?>    
          
<div id="pastpage2">                       
    <p5><?php echo $idunit ?></p5> 
    <p class="mb-0"><?php echo $code." : ".$unitName."----"."YEAR  ".$year." : SEMESTER".$semester ?></p>
    <p>INSTRUCTOR : <small><?php echo $instructor ?></small></p>
    <hr>

    <div class="row mb-2 ">
        <div class="col-md-4 pl-1 pt-3 pb-3 pr-1 bg-dark mr-auto ml-auto">   

            <button class="btn btn-info mb-1" id="stDocs" style="float:left">
                Uploaded Documents
            </button> 

            <button class="btn btn-secondary mb-1" id="stEnroll" style="float:left">
                Enrolled Students
            </button> 

            <button class="btn btn-info mb-1" id="stperform" style="float:left">
                Individual Students' Performance
            </button>

            <button class="btn btn-secondary mb-1" id="stGeneral" style="float:left">
                General Students Performance
            </button> 

            <button class="btn btn-info mb-1" id="stFeedback" style="float:left">
                Course Feedback
            </button>
        </div>
        <div class="col-md-8 bg-light">
            <div id="stFile">
                <?php 

                $idunit = $_SESSION["idunit"];
                include 'databasecon.php';
                $page_rows = 4; 
                $query=mysqli_query($connection,"select count(id) from asynos where unitid='$idunit'");
                include "pages.php";
                $nquery=mysqli_query($connection,"SELECT * FROM asynos where unitid='$idunit'   $limit");
                ?>
                
                
                        
                <table width="100%" border="9">
                    <tr>
                        <th>Title </th>
                        <th>File Name</th>
                        <th>File Size(KB)</th>
                        <th>View</th>
                    </tr>


                    <tr><?php echo $paginationCtrls; ?></tr>
                            <?php
                            
                            $bg = 0;
                            while($crow = mysqli_fetch_array($nquery)){
                                if ( $bg%2 == 0){
                                    $class="even";
                                }else{
                                    $class="light"; 
                                }
                                $bg++;
                                
                            ?>
                    <tr class="<?php echo $class; ?>">
                        <td><?php echo $crow['title'] ?></td>
                        <td><?php echo $crow['file'] ?></td>
                        <td><?php echo $crow['size'] ?></td>
                        <td><a href="viewfile.php?id=<?php echo $crow['id'] ?>">view file</a></td>
                        </tr>
                        <?php
                        }
                        ?>
                </table>                        

            </div>
            <div id="stEnrolled">
                <?php
                    $page_rows = 4; 
                    $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' ");
                include "pages.php";
                $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit'   $limit");
                ?>
                <div class="ml-5 mr-5">
                    <table border="9" cellspacing="0" cellpadding="5"  align="center">
                        <th colspan="6"><p5>ENROLLED STUDENTS: </p5> </th>
                        <tr><th>Username</th></tr>
                        <tr><?php echo $paginationCtrls; ?></tr>
                                <?php
                                    $bg = 0;
                                    while($crow = mysqli_fetch_array($nquery)){
                                        if ( $bg%2 == 0){
                                            $class="even";
                                        }else{
                                            $class="light"; 
                                        }
                                        $bg++;
                                        
                                    ?>
                        <tr class="<?php echo $class; ?>">
                            <td> <?php echo $crow['username'] ?></td>

                        </tr>
                        <?php
                        }
                        ?>
                        <tr><td colspan="3"><p4>Total No Of Enrolled Students: <?php echo $rows = $row[0]; ?></p4></td></tr>
                    </table>
                </div>
                <br>
            </div>
            <div id="EachSt">
                <?php
                $unitt = $_SESSION["unitName"];
                        
                $idunit = $_SESSION["idunit"];
                $page_rows = 4; 
                $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' ");
                include "pages.php";
                $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit'   $limit");
                
                include "sum.php";
                ?>
                <table border="5" cellspacing="1" cellpadding="1" width="80%" align="center">
                    <th colspan="6"><p5>Performance</p5></th>
                    <tr><th><p6>Username</p6></th><th><p6>Cat</p6></th><th><p6>Assignment</p6></th><th><p6>Main Exam</p6></th><th><p6>Total</p6></th><th><p6>Grades</p6></th></tr>
                    <tr><?php echo $paginationCtrls; ?></tr>
                            <?php
                                 $bg = 0;
                                 while($crow = mysqli_fetch_array($nquery)){
                                     if ( $bg%2 == 0){
                                         $class="even";
                                     }else{
                                         $class="light"; 
                                     }
                                     $bg++;
                                     
                                 ?>
                    <tr class="<?php echo $class; ?>">

                        <td><?php echo $crow['username'] ?></td>
                        <td><label><?php echo $crow['cat'];?> </label></td>
                        <td><label><?php echo $crow['assyn'];?>   </label></td>
                        <td><label><?php echo $crow['mainexam'];?> </label></td>
                        <td><label><?php echo $crow['total'];?></label></td>
                        <td><label><?php include 'remarks.php' ?>  </label></td>
                    </tr>
                    <?PHP } ?>
                    <tr><td> <div id="tick">Average</div></td>
                        <td><label><?php echo $sum=$row['total'];?></label></td>
                        <td><label><?php echo $sum=$row1['total1'];?>"  </label></td>
                        <td><label><?php echo $sum=$row2['total2'];?></label></td>
                        <td><label><?php echo $sum=$row3['total3'];?></label></td>
                        <td><label><?php echo $mygrade; ?></label></td>
                    </tr>                               
                </table>                           
            </div>
            <div id="Each_Perform">
                <?php
                $query=("SELECT * FROM grades WHERE  unitid= '$idunit' ")or die(mysqli_connect_error()); 
                $result=mysqli_query($connection,$query);
                if( mysqli_num_rows($result) > 1) {

                    '<br>';
                    include 'form.html';

                }else{

                    echo "NOTHING TO SHOW";

                } ?>
                <br><br>
            </div>
            <div id="stfeedbk">                           
                <?php 

                $query=("SELECT * FROM lecturers WHERE unitid = '$idunit'")or die (mysqli_connect_error()); 
                $result=mysqli_query($connection,$query);
                $row=mysqli_fetch_array($result);
                if( mysqli_num_rows($result) > 0) {

                    ?>
                           <div class="col-md-7">
                            <?php include "extract.html"; ?>
                           <div class="mt-3"> <?php include 'table.html';?></div>
                        
                    <?php


                }else{

                    echo "nothing to show" ;  

                } ?>

                <br>
            </div>
        </div>
    </div>
</div>
