<?php 
    $idunit = $_SESSION["idunit"];
    $code = $_SESSION["code"];
    $unitName = $_SESSION["unitName"];
    $year = $_SESSION["year"];
    $semester = $_SESSION["semester"];
    $instructor = $_SESSION["instructor"];
    ?>
<style>
    #viewfile2,#enrollStudents,#eachStdn2,#allPerform2,#feedbk2{
        display:none;
    }
        p{
            color:black!important;
            font-size:18px;
        }
    

</style>

<div id="pastpage2" class="mt-0">

    <p5><?php echo $idunit ?></p5> 
    <p class="mb-0"><?php echo $code." : ".$unitName?></p>
    <p>INSTRUCTOR : <small><?php echo $instructor ?></small></p>
    <hr>
    <div class="row mb-2 ">
        <div class="col-md-4 pl-1 pt-3 pb-3 pr-1 bg-dark mr-auto ml-auto">
            <button class="btn btn-info mb-1" id="docs2" style="float:left">
                Uploaded Documents
            </button> 

            <button class="btn btn-secondary mb-1" id="enroll2" style="float:left">
                Enrolled Students
            </button> 

            <button class="btn btn-info mb-1" id="perform2" style="float:left">
                Individual Students' Performance
            </button>

            <br>
            <button class="btn btn-secondary mb-1" id="general2" style="float:left">
                General Students Performance 
            </button>
            
            <button class="btn btn-info mb-1" id="btnFeedback2" style="float:left">
                Course Feedback
            </button>
            
        </div> 
        <div class="col-md-8 bg-light">
            <div id="viewfile2">
                <?php 
                $idunit = $_SESSION["idunit"];
                include 'databasecon.php';

                $page_rows = 4; 
                $query=mysqli_query($connection,"select count(id) from asynos where unitid='$idunit'");
                include "pages.php";
                $nquery=mysqli_query($connection,"SELECT * FROM asynos where unitid='$idunit'   $limit");
                ?><br>
                <table width="100%" border="5">
                    <tr>
                        <th>Title </th>
                        <th>File Name</th>
                        <th>File Size(KB)</th>
                        <th>View</th>
                    </tr>


                    <tr><?php echo $paginationCtrls; ?></tr>
                    <?php
                        $bg = 0;
                        while($crow = mysqli_fetch_array($nquery)){
                            if ( $bg%2 == 0){
                                $class="even";
                            }else{
                                $class="light"; 
                            }
                            $bg++;
                            
                        ?>
                    <tr class="<?php echo $class; ?>">
                        <td><?php echo $crow['title'] ?></td>
                        <td><?php echo $crow['file'] ?></td>
                        <td><?php echo $crow['size'] ?></td>
                        <td class="text-primary"><a target="_blank" href="viewfile.php?id=<?php echo $crow['id'] ?>">view file</a></td>
                    </tr>
                    <?php
                        }
                    ?>
                </table>                                            
            </div>
            <div id="enrollStudents">
                <?php
                $idunit = $_SESSION["idunit"];

                $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' ");
                $row = mysqli_fetch_row($query);

                $rows = $row[0];

                $page_rows = 5;

                $last = ceil($rows/$page_rows);

                if($last < 1){
                    $last = 1;
                }

                $pagenum = 1;

                if(isset($_GET['pn'])){
                    $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
                }

                if ($pagenum < 1) { 
                    $pagenum = 1; 
                } 
                else if ($pagenum > $last) { 
                    $pagenum = $last; 
                }

                $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;

                $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit'   $limit");

                $paginationCtrls = '';

                if($last != 1){

                if ($pagenum > 1) {
                    $previous = $pagenum - 1;
                    $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" class="btn btn-default">Previous</a> &nbsp; &nbsp; ';

                    for($i = $pagenum-4; $i < $pagenum; $i++){
                        if($i > 0){
                            $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-default">'.$i.'</a> &nbsp; ';
                        }
                    }
                }

                $paginationCtrls .= ''.$pagenum.' &nbsp; ';

                for($i = $pagenum+1; $i <= $last; $i++){
                    $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-default">'.$i.'</a> &nbsp; ';
                    if($i >= $pagenum+4){
                        break;
                    }
                }

                if ($pagenum != $last) {
                    $next = $pagenum + 1;
                    $paginationCtrls .= ' &nbsp; &nbsp; <a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" class="btn btn-default">Next</a> ';
                }
                }
                ?>
                <br>
                <div class="ml-5 mr-5">
                    <table border="1" cellspacing="0" cellpadding="1" width="50%" align="left" >
                        <th><h6>Enrolled Students</h6> </th>
                     
                        <tr><?php echo $paginationCtrls; ?></tr>
                                <?php
                                    $bg = 0;
                                    while($crow = mysqli_fetch_array($nquery)){
                                        if ( $bg%2 == 0){
                                            $class="light";
                                        }else{
                                            $class="even"; 
                                        }
                                        $bg++;
                                        
                                    ?>
                        <tr class="<?php echo $class; ?>">
                            <td> <?php echo $crow['username'] ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                        <tr><td><br>Total No Of Enrolled Students: <?php echo $rows = $row[0]; ?></td></tr>
                    </table>
                </div>
            </div>
            <div id="eachStdn2" >
                <?php
                $unitt = $_SESSION["unitName"];
                        
                $idunit = $_SESSION["idunit"];
                $query=mysqli_query($connection,"select count(id) from `enroll` WHERE unitid='$idunit' ");
                $row = mysqli_fetch_row($query);

                $rows = $row[0];

                $page_rows = 5;

                $last = ceil($rows/$page_rows);

                if($last < 1){
                    $last = 1;
                }

                $pagenum = 1;

                if(isset($_GET['pn'])){
                    $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
                }

                if ($pagenum < 1) { 
                    $pagenum = 1; 
                } 
                else if ($pagenum > $last) { 
                    $pagenum = $last; 
                }

                $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;

                $nquery=mysqli_query($connection,"select * from enroll WHERE unitid='$idunit'   $limit");

                $paginationCtrls = '';

                if($last != 1){

                if ($pagenum > 1) {
                    $previous = $pagenum - 1;
                    $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" class="btn btn-default">Previous</a> &nbsp; &nbsp; ';

                    for($i = $pagenum-4; $i < $pagenum; $i++){
                        if($i > 0){
                            $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-default">'.$i.'</a> &nbsp; ';
                        }
                    }
                }

                $paginationCtrls .= ''.$pagenum.' &nbsp; ';

                for($i = $pagenum+1; $i <= $last; $i++){
                    $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-default">'.$i.'</a> &nbsp; ';
                    if($i >= $pagenum+4){
                        break;
                    }
                }

                if ($pagenum != $last) {
                    $next = $pagenum + 1;
                    $paginationCtrls .= ' &nbsp; &nbsp; <a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" class="btn btn-default">Next</a> ';
                }
                }

                include "sum.php";
                ?>

                <table border="1" cellspacing="1" cellpadding="1" width="100%" align="center">
                    <th colspan="6"><p3> PERFORMANCE</p3></th>
                    <tr><th class="text-primary">Username</th>
                    <th class="text-primary">Cat</th>
                    <th class="text-primary">Assignment</th>
                    <th class="text-primary">Main exam</th>
                    <th class="text-primary">Total</th>
                    <th class="text-primary">Grades</th></tr>
                    <tr><?php echo $paginationCtrls; ?></tr>
                            <?php
                            $bg=0;
                                while($crow = mysqli_fetch_array($nquery)){
                                    if ( $bg%2 == 0){
                                        $class="light";
                                    }else{
                                        $class="even"; 
                                    }
                                    $bg++;
                                    
                                ?>
                    <tr class="<?php echo $class; ?>">

                        <td><?php echo $crow['username'] ?></td>
                        <td><label><?php echo $crow['cat'];?> </label></td>
                        <td><label><?php echo $crow['assyn'];?>   </label></td>
                        <td><label><?php echo $crow['mainexam'];?> </label></td>
                        <td><label><?php echo $crow['total'];?></label></td>
                        <td><label><?php include 'remarks.php' ?>  </label></td>
                    </tr>
                    <?PHP } ?>
                    <tr><td> <div id="tick">Average</div></td>
                        <td><label><?php echo round($sum=$row['total'],1);?></label></td>
                        <td><label><?php echo round($sum=$row1['total1'],1);?>  </label></td>
                        <td><label><?php echo round($sum=$row2['total2'],1);?></label></td>
                        <td><label><?php echo round($sum=$row3['total3'],1);?></label></td>
                        <td><label><?php echo $mygrade; ?></label></td></tr>
                        

                </table>
            </div>
            <div id="allPerform2">                                            
                <?php
                $query=("SELECT * FROM grades WHERE  unitid= '$idunit' ")or die(mysqli_connect_error()); 
                $result=mysqli_query($connection,$query);
                if( mysqli_num_rows($result) > 1) {
                
                    '<br>';
                    include 'form.html';
                    

                }else{

                    echo "NOTHING TO SHOW";
                
                } ?>
                <br>
            </div>
            <div id="feedbk2" class="row justify-content-center">
                                                                                        
                <?php                     
                $query=("SELECT * FROM lecturers WHERE unitid = '$idunit'")or die (mysqli_connect_error()); 
                $result=mysqli_query($connection,$query);
                $row=mysqli_fetch_array($result);
                if( mysqli_num_rows($result) > 0) {?>
                
                        <br>
                        <div class="col-md-7">
                            <?php include "extract.html"; ?>
                           <div class="mt-3"> <?php include 'table.html';?></div>
                            
                            
                        </div>

                    
                    <br>
                    <?php

                }else{

                    echo "nothing to show" ;  

                } ?>                                                    
                <br>
            </div>
        </div>
    </div>
</div>