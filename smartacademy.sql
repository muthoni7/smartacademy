-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 14, 2018 at 12:37 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smartacademy`
--

-- --------------------------------------------------------

--
-- Table structure for table `academics`
--

DROP TABLE IF EXISTS `academics`;
CREATE TABLE IF NOT EXISTS `academics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `semester` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academics`
--

INSERT INTO `academics` (`id`, `username`, `year`, `semester`) VALUES
(1, 'john', '1', '1'),
(2, 'peter', '1', '1'),
(3, 'mary', '1', '1'),
(4, 'janet', '1', '1'),
(5, 'lucy@gmail.com', '1', '1'),
(6, 'john@gmail.com', '1', '1'),
(7, 'peter@gmail.com', '1', '1'),
(8, 'janet@gmail.com', '1', '1'),
(9, 'luke@gmail.com', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `asynos`
--

DROP TABLE IF EXISTS `asynos`;
CREATE TABLE IF NOT EXISTS `asynos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `unitid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `yeart` varchar(255) NOT NULL,
  `unitt` varchar(255) NOT NULL,
  `img` longblob NOT NULL,
  `uploaddate` varchar(255) NOT NULL,
  `enddate` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asynos`
--


-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userfrom` varchar(255) NOT NULL,
  `userto` varchar(255) NOT NULL,
  `msg` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `userfrom`, `userto`, `msg`) VALUES
(1, 'jackie', 'ireri', 'Hello'),
(2, 'ireri', 'jackie', 'Hello too'),
(3, 'jackie', 'ireri', 'how is you'),
(4, 'ireri', 'jackie', 'Am fine thankyou'),
(5, 'jackie@gmail.com', 'ireri@gmail.com', 'hello'),
(6, 'lucy@gmail.com', 'jackie@gmail.com', 'hello'),
(7, 'jackie@gmail.com', 'ireri@gmail.com', 'Hello'),
(8, 'james@gmail.com', 'jackie@gmail.com', 'Hello'),
(9, 'jackie@gmail.com', 'lucy@gmail.com', 'hello too Lucy'),
(10, 'john@gmail.com', 'jackie@gmail.com', 'hello'),
(11, 'ireri@gmail.com', 'jackie@gmail.com', 'hello too');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
CREATE TABLE IF NOT EXISTS `data` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `unitid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `unitt` varchar(255) NOT NULL,
  `field` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `yeart` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `unitid`, `username`, `unitt`, `field`, `details`, `yeart`) VALUES
(40, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'availability_of_study_materials', '', '                                             1 '),
(39, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'student_puntuality', '', '                                             1 '),
(38, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'student_cooperation', '', '                                             1 '),
(37, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'student_attendance', '', '                                             1 '),
(36, 'unit-0019', 'ireri@gmail.com', 'HURI 111', 'benefit earned to students', '4', '                                             1 '),
(35, 'unit-0019', 'ireri@gmail.com', 'HURI 111', 'usefulness_of_tests', '4', '                                             1 '),
(34, 'unit-0019', 'ireri@gmail.com', 'HURI 111', 'availability_of_study_materials', '4', '                                             1 '),
(33, 'unit-0019', 'ireri@gmail.com', 'HURI 111', 'student_puntuality', '3', '                                             1 '),
(32, 'unit-0019', 'ireri@gmail.com', 'HURI 111', 'student_cooperation', '2', '                                             1 '),
(31, 'unit-0019', 'ireri@gmail.com', 'HURI 111', 'student_attendance', '1', '                                             1 '),
(30, 'unit-0012', 'ireri@gmail.com', 'BICT 122', 'benefit earned to students', '1', '                                             3 '),
(29, 'unit-0012', 'ireri@gmail.com', 'BICT 122', 'usefulness_of_tests', '2', '                                             3 '),
(28, 'unit-0012', 'ireri@gmail.com', 'BICT 122', 'availability_of_study_materials', '4', '                                             3 '),
(27, 'unit-0012', 'ireri@gmail.com', 'BICT 122', 'student_puntuality', '3', '                                             3 '),
(26, 'unit-0012', 'ireri@gmail.com', 'BICT 122', 'student_cooperation', '2', '                                             3 '),
(25, 'unit-0012', 'ireri@gmail.com', 'BICT 122', 'student_attendance', '1', '                                             3 '),
(41, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'usefulness_of_tests', '', '                                             1 '),
(42, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'benefit earned to students', '', '                                             1 '),
(43, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'student_attendance', '', '                                             1 '),
(44, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'student_cooperation', '', '                                             1 '),
(45, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'student_puntuality', '', '                                             1 '),
(46, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'availability_of_study_materials', '', '                                             1 '),
(47, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'usefulness_of_tests', '', '                                             1 '),
(48, 'unit-0013', 'ireri@gmail.com', 'BICT 123', 'benefit earned to students', '', '                                             1 ');

-- --------------------------------------------------------

--
-- Table structure for table `enroll`
--

DROP TABLE IF EXISTS `enroll`;
CREATE TABLE IF NOT EXISTS `enroll` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `unitid` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `unit_name` varchar(50) NOT NULL,
  `unit_code` varchar(50) NOT NULL,
  `year` varchar(50) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `cat` varchar(255) NOT NULL,
  `assyn` varchar(255) NOT NULL,
  `mainexam` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `grades` varchar(255) NOT NULL,
  `lecturer` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enroll`
--

INSERT INTO `enroll` (`id`, `unitid`, `username`, `unit_name`, `unit_code`, `year`, `semester`, `cat`, `assyn`, `mainexam`, `total`, `grades`, `lecturer`, `status`) VALUES
(1, 'unit-0011', 'john', 'PROCEDUARAL PROGRAMMING', 'BICT 121', '1', '1', '0', '0', '0', '0', '', 'ireri', ''),
(2, 'unit-0012', 'john', 'QUANTUM COMPUTING', 'BICT 122', '1', '1', '5', '10', '50', '65', 'B', 'ireri@gmail.com', ''),
(3, 'unit-0019', 'peter@gmail.com', ' HUMAN RIGHTS', 'HURI 111', '1', '1', '1', '3', '5', '9', 'F', 'ireri@gmail.com', ''),
(4, 'unit-0019', 'lucy@gmail.com', ' HUMAN RIGHTS', 'HURI 111', '1', '1', '5', '15', '55', '75', 'A', 'ireri@gmail.com', ''),
(5, 'unit-0019', 'janet@gmail.com', ' HUMAN RIGHTS', 'HURI 111', '1', '1', '9', '13', '41', '63', 'B', 'ireri@gmail.com', ''),
(6, 'unit-0019', 'luke@gmail.com', ' HUMAN RIGHTS', 'HURI 111', '1', '1', '8', '6', '20', '34', 'F', 'ireri@gmail.com', ''),
(7, 'unit-0013', 'lucy@gmail.com', 'COMPUTER ORGANIZATION AND ACHTECTURE', 'BICT 123', '1', '1', '0', '0', '0', '0', '', 'ireri@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `regno` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `feedback` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `regno`, `username`, `profession`, `feedback`) VALUES
(3, 'L0011', 'jackie', 'Admin', ' Hello'),
(2, 'L0012', 'ireri', 'lecturer', ' GOOD STUFF'),
(4, 'S0013', 'john@gmail.com', 'student', ' Thankyou');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
CREATE TABLE IF NOT EXISTS `grades` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `unitid` varchar(255) NOT NULL,
  `unitname` varchar(255) NOT NULL,
  `lecreg` varchar(255) NOT NULL,
  `grade` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `yeart` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `unitid`, `unitname`, `lecreg`, `grade`, `number`, `yeart`) VALUES
(1, 'unit-0019', ' HUMAN RIGHTS ', 'ireri@gmail.com', 'A', '1', ''),
(2, 'unit-0019', ' HUMAN RIGHTS ', 'ireri@gmail.com', 'B', '1', ''),
(3, 'unit-0019', ' HUMAN RIGHTS ', 'ireri@gmail.com', 'C', '0', ''),
(4, 'unit-0019', ' HUMAN RIGHTS ', 'ireri@gmail.com', 'D', '0', ''),
(5, 'unit-0019', ' HUMAN RIGHTS ', 'ireri@gmail.com', 'F', '2', ''),
(13, 'unit-0012', 'QUANTUM COMPUTING ', 'ireri@gmail.com', 'F', '0', ''),
(12, 'unit-0012', 'QUANTUM COMPUTING ', 'ireri@gmail.com', 'D', '0', ''),
(11, 'unit-0012', 'QUANTUM COMPUTING ', 'ireri@gmail.com', 'C', '0', ''),
(10, 'unit-0012', 'QUANTUM COMPUTING ', 'ireri@gmail.com', 'B', '1', ''),
(14, 'unit-0012', 'QUANTUM COMPUTING', 'ireri@gmail.com', 'A', '0', ''),
(22, 'unit-0013', 'COMPUTER ORGANIZATION AND ACHTECTURE ', 'ireri@gmail.com', 'F', '0', ''),
(21, 'unit-0013', 'COMPUTER ORGANIZATION AND ACHTECTURE ', 'ireri@gmail.com', 'D', '0', ''),
(20, 'unit-0013', 'COMPUTER ORGANIZATION AND ACHTECTURE ', 'ireri@gmail.com', 'C', '0', ''),
(19, 'unit-0013', 'COMPUTER ORGANIZATION AND ACHTECTURE ', 'ireri@gmail.com', 'B', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `lecturers`
--

DROP TABLE IF EXISTS `lecturers`;
CREATE TABLE IF NOT EXISTS `lecturers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `unitid` varchar(255) NOT NULL,
  `unitt` varchar(255) NOT NULL,
  `styear` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `lecturername` varchar(255) NOT NULL,
  `student_attendance` varchar(255) NOT NULL,
  `student_cooperation` varchar(255) NOT NULL,
  `student_puntuality` varchar(255) NOT NULL,
  `availability_of_study_materials` varchar(255) NOT NULL,
  `usefulness_of_tests` varchar(255) NOT NULL,
  `unit_benefit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lecturers`
--

INSERT INTO `lecturers` (`id`, `unitid`, `unitt`, `styear`, `semester`, `lecturername`, `student_attendance`, `student_cooperation`, `student_puntuality`, `availability_of_study_materials`, `usefulness_of_tests`, `unit_benefit`) VALUES
(6, 'unit-0019', 'HURI 111', '                                             1 ', ' 1', 'ireri@gmail.com', '1', '2', '3', '4', '4', '4'),
(5, 'unit-0012', 'BICT 122', '                                             3 ', ' 1', 'ireri@gmail.com', '1', '2', '3', '4', '2', '1'),
(8, 'unit-0013', 'BICT 123', '                                             1 ', ' 1', 'ireri@gmail.com', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `rjstrdb`
--

DROP TABLE IF EXISTS `rjstrdb`;
CREATE TABLE IF NOT EXISTS `rjstrdb` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `profession` varchar(255) NOT NULL,
  `usertyp` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `phoneno` varchar(255) NOT NULL,
  `regno` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `aptdby` varchar(255) NOT NULL,
  `nusertyp` varchar(255) NOT NULL,
  `removedby` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `regcode` varchar(255) NOT NULL,
  `codeconfm` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rjstrdb`
--

INSERT INTO `rjstrdb` (`id`, `profession`, `usertyp`, `fullname`, `phoneno`, `regno`, `username`, `password`, `aptdby`, `nusertyp`, `removedby`, `status`, `regcode`, `codeconfm`, `image`) VALUES
(1, 'lecturer', 'admin', 'jackline', '0707802693', 'L0011', 'jackie@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '', '', '', 'approved', '5124', 'confirmed', 'IMG_20170122_121739.jpg'),
(2, 'lecturer', 'user', 'Madam Ireri', '07451269853', 'L0012', 'ireri@gmail.com', 'bb485d728dffa84728308543c2eccecb4b339841', 'jackie@gmail.com', 'lecturer', 'jackie@gmail.com', 'approved', '8451', 'confirmed', '1485200904449.jpg'),
(3, 'student', 'user', 'John Mark', '0741852963', 'S0013', 'john@gmail.com', 'a51dda7c7ff50b61eaea0444371f4a6a9301e501', '', '', '', 'approved', '8172', 'confirmed', '$RC1FMHK.jpg'),
(4, 'student', 'user', 'Peter James', '0745952155', 'S0014', 'peter@gmail.com', '4b8373d016f277527198385ba72fda0feb5da015', '', '', '', 'approved', '8521', 'confirmed', ''),
(5, 'student', 'user', 'Mary M', '0784512954', 'S0015', 'mary@gmail.com', '5665331b9b819ac358165f8c38970dc8c7ddb47d', '', '', '', 'approved', '0215', NULL, ''),
(6, 'student', 'user', 'janet kim', '0784541245', 'S0016', 'janet@gmail.com', 'd92c2797bf00c69760726b459e36d1941c2d6ec7', '', '', '', 'approved', '9632', 'confirmed', ''),
(7, 'student', 'user', 'Lucy brown', '074512963', 'S0017', 'lucy@gmail.com', '474e97d07b83ea9b34d1ec399840354182f3b6c1', '', '', '', 'approved', '7412', 'confirmed', 'FB_20161017_10_28_01_Saved_Picture.jpg'),
(8, 'student', 'user', 'Ryan mike', '0784984895', 'S0018', 'ryan@gmail.com', 'ea3cd978650417470535f3a4725b6b5042a6ab59', '', '', '', 'pending', '8524', NULL, ''),
(9, 'student', 'user', 'Luke Mark', '0741854123', 'S0019', 'luke@gmail.com', '6b3799be4300e44489a08090123f3842e6419da5', '', '', '', 'approved', '8745', 'confirmed', ''),
(10, 'lecturer', 'admin', 'Sir Karume', '07418521456', 'L00110', 'karume@gmail.com', '239935177c5db9a792a9ea3e42ca57f34a9bfdf0', 'jackie@gmail.com', 'lecturer', 'jackie@gmail.com', 'approved', '8956', 'confirmed', 'FB_20161017_10_28_01_Saved_Picture.jpg'),
(11, 'lecturer', 'user', 'MR MINDO', '0784512963', 'L00111', 'mindo@gmail.com', '63c040124cadb6fe548818964830a82bb134a3ee', 'jackie', 'lecturer', 'jackie', 'approved', '8541', 'confirmed', ''),
(12, 'lecturer', 'user', 'MR KIRORI', '0798345234', 'L00112', 'kirori@gmail.com', '67624b66d5cd74d475e669b6141528d52156e177', '', '', '', 'approved', '6352', 'confirmed', 'Gs24aecc00-981e-439f-81cc-ae9b46bb0dd8.jpg'),
(17, 'student', 'user', 'Joyce Me', '+254784512478', 'S00117', 'joyce@gmail.com', 'bf6c9482ce636dbfda2f480e6037d048ddd6c509', '', '', '', 'approved', '7753', 'confirmed', NULL),
(16, 'student', 'user', 'Moris Mo', '+254707802693', 'S00116', 'moris@gmail.com', '77f4d0d5df28448cb22f13e724f1b5f3c0413d2c', '', '', '', 'approved', '9055', 'confirmed', NULL),
(18, 'lecturer', 'user', 'James jj', '+254784512471', 'L00118', 'james@gmail.com', '474ba67bdb289c6263b36dfd8a7bed6c85b04943', '', '', '', 'approved', '6792', 'confirmed', 'IMG_20170310_122319.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `stdata`
--

DROP TABLE IF EXISTS `stdata`;
CREATE TABLE IF NOT EXISTS `stdata` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `stdname` varchar(255) NOT NULL,
  `lec` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `field` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stdoc`
--

DROP TABLE IF EXISTS `stdoc`;
CREATE TABLE IF NOT EXISTS `stdoc` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `lecturer` varchar(255) NOT NULL,
  `file` longblob NOT NULL,
  `uploaddate` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stdoc`
--



-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `year` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `unitcode` varchar(255) NOT NULL,
  `unitname` varchar(255) NOT NULL,
  `LECTURER` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `idunit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `year`, `semester`, `unitcode`, `unitname`, `LECTURER`, `status`, `idunit`) VALUES
(2, '3', '1', 'BICT 122', 'QUANTUM COMPUTING', 'ireri@gmail.com', 'ended', 'unit-0012'),
(3, '1', '1', 'BICT 123', 'COMPUTER ORGANIZATION AND ACHTECTURE', 'ireri@gmail.com', '', 'unit-0013'),
(5, '1', '1', 'ECON 111', 'PRINCIPLES OF MICRO ECONOMICS', 'kirori@gmail.com', '', 'unit-0015'),
(9, '1', '1', 'HURI 111', ' HUMAN RIGHTS', 'ireri@gmail.com', '', 'unit-0019'),
(12, '1', '1', 'MATH 100', ' General Maths', 'mindo@gmail.com', '', 'unit-00112');

-- --------------------------------------------------------

--
-- Table structure for table `unitsdata`
--

DROP TABLE IF EXISTS `unitsdata`;
CREATE TABLE IF NOT EXISTS `unitsdata` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `regno` varchar(255) NOT NULL,
  `unitt` varchar(255) NOT NULL,
  `A_score` varchar(255) NOT NULL,
  `B_score` varchar(255) NOT NULL,
  `C_score` varchar(255) NOT NULL,
  `D_score` varchar(255) NOT NULL,
  `F_score` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
