

<html>
    <head>
        <title>SmartAcademy</title>

            <style>
            #lecfile,#enrolledStudents,#lecEachStdn,#lecAllPerform,#lecfeedbk{
                display: none;
                font-size: 20px;
            }
                
        </style>
    </head>
    <body>
    <?php include "leftnav.php";
    include "databasecon.php";
    session_start(); ?> 
        
            <div class="row justify-content-center ">
                <div class="col-md-12  " id="backbg">
                    <div class="row ">
                        <div class="col-md-12">
                            <section>
                                <?php
                               echo $label = $_SESSION["label"];                        
                                if ($label == 'lecpast') { 
                                    $page_rows = 5; 
                                    $query=mysqli_query($connection,"select count(id) from `units` WHERE status='ended' ");
                                    include "pages.php";
                                    $nquery=mysqli_query($connection,"select * from `units` WHERE status='ended'   $limit");
                                    ?>
                                    <form METHOD="POST">
                                            <?php
                                        if(isset($_POST['search'])){ 
                                                ?>

                                            <br>
                                            <td><input type="text" name="id"  maxlength="20" /required placeholder="enter unit's id"></td>
                                            <td><input type="submit" name="search" value="SEARCH"/><br><br><br></td>

                                            <?php 
                                                $id=$_POST['id'];
                                                $query= ("SELECT *FROM `units` WHERE status='ended' and id='$id'");
                                                $result=mysqli_query($connection,$query);
                                            ?>

                                            <table border="1" align="center" width="80%">
                                                <th colspan="7"><p5>PAST UNITS</p5></th>
                                                <tr><th><p6>Units' Id</p6></th><th><p6>Year</p6></th><th><p6>Semester</p6></th><th><p6>Unit Code</p6></th><th><p6>Unit Name</p6></th></tr>

                                                <?php
                                                while($crow = mysqli_fetch_array($result))
                                                {
                                                ?>
                                                <tr>
                                                    
                                                    <td> <?php echo $crow['idunit'] ?></td>
                                                    <td> <?php echo $crow['year'] ?></td>
                                                    <td> <?php echo $crow['semester'] ?></td>
                                                    <td> <?php echo $crow['unitcode'] ?></td>
                                                    <td> <?php echo $crow['unitname'] ?></td>

                                                    <td>


                                                        <a href="next.php?unitt=<?php echo $crow["unitcode"];?>&unitid=<?php echo $crow["id"]; ?>&redirect=<?php echo "SINGLE"; ?>"class="link">INDIVIDUAL STUDENT PERFORMANCE</a>
                                                        <br>

                                                        <div id="tick1" align="right">
                                                        <a href="next.php?unitt=<?php echo $crow["unitcode"];?>&&unitid=<?php echo $crow["id"]; ?>&yeart=<?php echo $crow["year"]; ?>&semester=<?php echo $crow["semester"];?>&redirect=<?php echo "ended"; ?>"><h5>REACTIVATE THE UNIT</h5></a></div>
                                                    </td>

                                                    <?php } ?>

                                                </tr>
                                            </table>
                                            <?php
                                        } else {
                                            ?>

                                            <td><input type="text" name="id"  maxlength="20" /required placeholder="enter unit's id"></td>
                                            <td><input type="submit" name="search" value="SEARCH"></td>



                                            <table border="1" width="100%" cellpadding="10" >
                                                <th colspan="7"><p5>UNITS DONE BEFORE</p5></th>

                                                <tr><th>Unit's System ID</th><th>Lecturer</th><th>Year</th><th>Unit Code</th><th>Unit Name</th><th>Have An Over-View</th></tr>

                                                <?php

                                                $bg = 0;
                                                while($row1 = mysqli_fetch_array($nquery)){
                                                    if ( $bg%2 == 0){
                                                        $class="light";
                                                    }else{
                                                        $class="even"; 
                                                    }
                                                    $bg++;

                                                ?>
                                                <tr class="<?php echo $class; ?>">
                                                    <td> <?php echo $row1['idunit'] ?></td>
                                                    <td> <?php echo $row1['LECTURER'] ?></td>
                                                    <td> <?php echo $row1['year']?></td>
                                                    <td> <?php echo $row1['unitcode'] ?></td>
                                                    <td> <?php echo $row1['unitname'] ?></td>
                                                    <td style="color:blue; font-size:20px;"><a href="lecnew.php?label=overView&&idunit=<?php echo $row1['idunit'];?>&&code=<?php echo $row1['unitcode'];?>&&unitName=<?php echo $row1['unitname']; ?>&&year= <?php echo $row1['year'];?>&&semester=<?php echo $row1['semester'];?>&&instructor=<?php echo $row1['LECTURER'];?>">View</a>
                                                    <br>
                                                    </td>


                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </table>
                                            <div id="pages"><?php echo $paginationCtrls; ?></div>
                                            <?php
                                        } ?>

                                    </form>
                                    <?php
                                    
                                } 
                                else if($label == 'lecOverView'){
                                   
                                    include "lecOverView.php";
                                }
                                ?>
                            </section>
                        </div>
                        
                    </div>
                    
                </div>
               <!-- <?php include "trend.php"; ?>  -->
            </div>
            </nav>
        </div>
        </div>
        </div>
        <?php include "footer.php"; ?>
        </div>
    </body>
    <script src="../js/formscript.js"></script>
</html>