$(document).ready(
    
    
    function(){      
      
        $("#regform").on('submit',(function(e) {
            e.preventDefault();
            $('.professionerror').html("");
            $('.fullnameerror').html("");
            $('.emailerror').html("");
            $('.passworderror').html("");
            $('.cpasserror').html("");
            $('.phonenoerror').html("");
            $('.emailused').html("");
            
            $.ajax({
                url: "regcon.php", 	// Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(res)   // A function to be called if request succeeds
                {
                    
                    if (res.profession) {
                        $('.professionerror').html(res.profession);
                    }

                    if (res.fullname) {
                        $('.fullnameerror').html(res.fullname);
                    }

                    if (res.email){    
                        $('.emailerror').html(res.email);
                    }
                    if (res.password){
                        $('.passworderror').html(res.password);
                    }
                    if (res.cpass){
                        $('.cpasserror').html(res.cpass);
                    }
                    if (res.Phoneno){
                        $('.phonenoerror').html(res.Phoneno);
                    }
                    if (res.Email_Used){    
                        $('.emailused').html(res.Email_Used);
                    }
                  
                    if (res.Error){
                        swal("SORRY","REGISTRATION FAILED","error");
                    }
                    if (res.Success){
                            if (res.profession=="student"){
                                swal("REGISTRATION SUCCESSFULL","Please wait to be redirected...","success",{
                                    buttons:false, 
                                });
                                setTimeout("window.location.href='studentspage.php'", 4000);
                            }
                            if (res.profession=="lecturer"){
                                swal("REGISTRATION SUCCESSFULL","Please wait to be redirected...","success",{
                                    buttons:false,
                                });
                                setTimeout("window.location.href='lechome.php'", 4000);
                            }
                        
                    }
                }
            });
        }));

        $("#loginpage").on('submit',(function(e) {
            e.preventDefault();
            
            $('.emailerror').html("");
            $('.passworderror').html("");
            $.ajax({
                url: "../logincon.php", 	// Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(res)   // A function to be called if request succeeds
                {
                    
                   
                    if (res.email) {
                        
                        $('.emailerror').html(res.email);
                    }
                    if (res.Password) {
                        $('.passworderror').html(res.Password);
                    }
                    if (res.Error) {
                        swal("SORRY","Account does not exist","error");
                    }
                    if (res.Success) {
                        if (res.usertype =="user" && res.profession=="lecturer"){
                            swal(res.Success,"Please wait to be redirected...","success",{
                                buttons:false,
                            });
                            setTimeout("window.location.href='lechome.php'", 4000);
                        }
                        if (res.usertype =="user" && res.profession=="student"){
                            swal(res.Success,"Please wait to be redirected...","success",{
                                buttons:false, 
                            });
                            setTimeout("window.location.href='studentspage.php'", 4000);
                        }
                        if (res.usertype =="admin" && res.profession=="lecturer"){
                            swal(res.Success,"Please wait to be redirected...","success",{
                                buttons:false,
                            });
                            setTimeout("window.location.href='admhom.php'", 4000);
                        }
                    }
                }
            });
        }));
        $("#codepage").on('submit',(function(e) {
            e.preventDefault();
            
            $('.codeerror').html("");
            $.ajax({
                url: "../codecon.php", 	// Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(res)   // A function to be called if request succeeds
                {
                    
                   
                    if (res.code) {
                        $('.codeerror').html(res.code);
                    }
                    if (res.Error) {
                        swal("SORRY","Wrong Code","error");
                    }
                    if (res.Success) {
                        if (res.usertype =="user" && res.profession=="lecturer"){
                            swal(res.Success,"Please wait to be redirected...","success",{
                                buttons:false,
                            });
                            setTimeout("window.location.href='lechome.php'", 4000);
                        }
                        if (res.usertype =="user" && res.profession=="student"){
                            swal(res.Success,"Please wait to be redirected...","success",{
                                buttons:false, 
                            });
                            setTimeout("window.location.href='studentspage.php'", 4000);
                        }
                        if (res.usertype =="admin" && res.profession=="lecturer"){
                            swal(res.Success,"Please wait to be redirected...","success",{
                                buttons:false,
                            });
                            setTimeout("window.location.href='admhom.php'", 4000);
                        }
                    }
                }
            });
        }));
    }
);

function redirection(){
    window.location.href='profile.php';
}

function checkLength(len,ele){
    $('.fullnameerror').html("");
    var fieldLength = ele.value.length;
    if(fieldLength <= 2){
        $('.fullnameerror').html("fullname must be more than 3 characters");
    }
    else if(fieldLength > len){
        var str = ele.value;
        str = str.substring(0, str.length - 1);
        ele.value = str;
        }
    else{
        return true;
    }
};
