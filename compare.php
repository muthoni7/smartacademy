<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script src="../js/graf.js"></script>
    <script src="../js/grafJquery.js"></script>
	

    <script type="text/javascript">
        $(document).ready(function () {

            $.getJSON("comparedata.php", function (result) {

                var chart = new CanvasJS.Chart("mychart", {
                    data: [
                        {
						type: "splineArea",
                            dataPoints: result
                        }
                    ]
                });

                chart.render();
            });
        });
    </script>
</head>
<body>

<div id="container1">

<P3><?php echo "UNIT-ID: "; echo  $_SESSION["idunit"] = $idunit;?>&nbsp;&nbsp;&nbsp;&nbsp;<?php    echo $unit1 = $_SESSION["unitname"];?></P3>
<br>
    <div id="mychart" style="width: 350px; height: 300px; float:left;"></div>
</div>
</body>
</html>