<?php 
include "checkIn.php";
?>
<html>
    <head>
        <link rel="stylesheet" href="../css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../css/bootstrap4/js/bootstrap.min.js"></script>
        <script src="js/sweetalert.min.js"></script> 
        <style>
            body{
                max-height:20vh!important;
            }
        </style>
    </head>

    <body >
        <form method="post" id="codepage">
        <?php 
        session_start();
            include "databasecon.php";
            $username = $_SESSION["username"];
            $sql = "SELECT regcode,phoneno FROM rjstrdb WHERE username = '$username' ";
            $r = mysqli_query($connection, $sql);
            $row = mysqli_fetch_assoc($r);
            $random_hash=$row['regcode'];
            $phone=$row['phoneno'];
            include('sendsms.php'); 
        ?>
            <div class="phoneno bg-light mr-auto ml-auto">
                <h6><?php 
                        error_reporting(0);
                        echo $_GET["txtmsg"];
                    ?>
                </h6><br>
                <h5 >Please Enter The 4-Digit Code Sent To Your Phone Number</h5>
                <h6 class="text-danger codeerror"></h6>
                <input type="text" name="code">
                <div class="row justify-content-center mt-3">
                    <div class="col-md-4">
                        <button class="btn btn-danger btn-md" type="reset" name="cancel">Cancel</button>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-success btn-md" type="submit" name="submit">Submit</button>
                    </div>
                     
                </div>
                <hr>
                <div class="row justify-content-center mt-3">
                    <div class="col-md-12">
                        <a href="code.php?txtmsg=Code has been resent"><h6>Resend Code</h6></a>
                    </div>
                </div>
            </div>
        </form>
    </body>
    <script src="js/formValidation.js"></script>
</html>