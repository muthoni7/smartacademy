

<html>
    <head>
        <title>SmartAcademy</title>

        <link rel="stylesheet" type="text/css" href="../css/animate/animate.min.css">
        <link rel="stylesheet" href="../css/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/msg.css">
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../css/bootstrap4/js/bootstrap.min.js"></script>
        

            <style>
            #adminlnks,#leclnks,#stndlnks,#unitslnks,#fbklnks,#profilelnks{
                display: none;
                font-size: 20px;
                margin-top: 50px
            }
                
        </style>
    </head>
    <body>
        <?php include "adminLeftnav.php"; ?> 
            <div class="row justify-content-center">
                <div class="col-md-12 " >
                    <div class="row ">
                        <div class="col-md-12 ">
                            
                                <?php
                               echo $label = $_SESSION["label"];
                                                        
                                if($label=='feedback'){
                                    $page_rows = 5; 
                                    $query=mysqli_query($connection,"select count(id) from `feedback` WHERE  profession != 'Admin'");
                                    include "pages.php";
                                    $nquery=mysqli_query($connection,"select * from feedback WHERE  profession != 'Admin'   $limit");
                                    ?>
                                   
                                    <table border="1" cellspacing="0" cellpadding="1" width="60%" align="center">
                                        <th colspan="4">FEEDBACK</th>
                                        <tr><th></th><th>Username</th><th>Profession</th><th>Feedback</th></tr>

                                                <?php
                                                    while($crow = mysqli_fetch_array($nquery)){
                                                ?>
                                        <tr>
                                            <td> <?php
                                                $newuser = $crow['username'];
                                                $nquerynew=mysqli_query($connection,"select * from rjstrdb WHERE  username ='$newuser'   ");
                                                $crownew = mysqli_fetch_array($nquerynew);
                                                if ( $crownew['image'] != ''){
                                                    ?> 
                                                        <img src="../images/<?php echo($crownew['image']);?>" class="rounded-circle" style="width:50px;height:50px;">
                                                    <?php  }else{
                                                        ?> 
                                                        <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                            <?php
                                                    } ?>
                                            </td>
                                            <td> <?php echo $crow['username']; echo " : "; echo $crow['regno']; ?></td>
                                            <td> <?php echo $crow['profession'] ?></td>
                                            <td> <?php echo $crow['feedback'] ?></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>

                                    </table>
                                    <div id="pages"><?php echo $paginationCtrls; ?></div>
                                    <?php
                                } 
                    
                                else if ($label == 'chat'){
                                    $page_rows = 5; 
                                    $query=mysqli_query($connection,"select count(id) from `rjstrdb` WHERE  status='approved' and username!='$username' ");
                                    include "pages.php";
                                    $nquery=mysqli_query($connection,"select * from rjstrdb where status='approved' and username!='$username'   $limit");
                                    ?>                   
                                    <form METHOD="POST">
                                        
                                        <?php
                                        if(isset($_POST['submit'])){
                                            ?>
                                            <br>
                                            <td><input type="text" name="lect" minlength="3" maxlength="20" /required placeholder="enter users' system id"></td>
                                            <td><input type="submit" name="submit" value="SEARCH"/><br><br><br></td>

                                            <?php	
                                            $lect=$_POST['lect'];
                                            $query= ("SELECT * FROM rjstrdb WHERE regno='$lect'  and status='approved' and username!='$username' ");
                                            $result=mysqli_query($connection,$query);
                                            ?>

                                            <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                                <tr><th colspan='6' >IDNO: <?php echo $lect; ?> </th></tr>
                                                <tr><th></th><th>Fullname</th><th>Username</th><th>Users' system id</th><th>Phone no.</th><th></th></tr>



                                                <?php
                                                while($crow = mysqli_fetch_array($result))
                                                {
                                                    ?>
                                                <tr>
                                                    <td> <?php if ( $crow['image'] != ''){
                                                            ?> 
                                                                <img src="../images/<?php echo($crow['image']);?>" class="rounded-circle" style="width:50px;height:50px;">
                                                            <?php  }else{
                                                                ?> 
                                                                <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                                    <?php
                                                            } ?>
                                                    </td>
                                                    <td> <?php echo $crow['fullname'] ?></td>
                                                    <td> <?php echo $crow['username'] ?></td>
                                                    <td> <?php echo $crow['regno'] ?></td>
                                                    <td> <?php echo $crow['phoneno'] ?></td>
                                                    <td><a href="new.php?label=chatpag&&username1=<?php echo $crow["username"]; ?>">
                                                        <img src="../images/chat.jpg"  style="width:150px;height:42px;border:0">
                                                    </a></td>
                                                </tr>
                                            </table>
                                            <?php
                                            }
                                        } 
                                        else {
                                            ?>
                                            <td><input type="text" name="lect" minlength="3" maxlength="200" /required placeholder="enter users' system id">
                                            <input type="submit" name="submit" value="SEARCH"></td>
                                            <br>
                                                                    
                                    </form>

                                    <table border="1" cellspacing="0" cellpadding="1" width="60%" align="center">
                                        <th colspan="6"><p5>CHAT</p5></th>
                                        <tr><th></th><th>Fullname</th><th>Username</th><th>Users' system id</th><th>Phone number</th><th></th></tr>

                                        <?php
                                            $bg = 0;
                                            while($crow = mysqli_fetch_array($nquery)){
                                                if ( $bg%2 == 0){
                                                    $class="light";
                                                }else{
                                                    $class="even"; 
                                                }
                                                $bg++;
                                        ?>
                                        <tr class="<?php echo $class; ?>">
                                            <td> <?php if ( $crow['image'] != ''){
                                                ?> 
                                                    <img src="../images/<?php echo($crow['image']);?>" class="rounded-circle" style="width:35px;height:35px;">
                                                <?php  }else{
                                                    ?> 
                                                    <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                        <?php
                                                } ?>
                                            </td>
                                            <td> <?php echo $crow['fullname'] ?></td>
                                            <td> <?php echo $crow['username'] ?></td>
                                            <td> <?php echo $crow['regno'] ?></td>
                                            <td> <?php echo $crow['phoneno'] ?></td>
                                            <td><a href="new.php?label=chatpag&&username1=<?php echo $crow["username"]; ?>">
                                                <img src="../images/chat.jpg"  style="width:150px;height:42px;border:0">
                                            </a></td>
                                        </tr>
                                        <?php
                                        }}
                                        ?>

                                    </table>
                                    <div id="pages"><?php echo $paginationCtrls; ?></div>                                  
                                        
                                    <?php 
                                }           

                                else if ($label=='profile'){

                                    if (isset($_POST['submit'])){
                                        $id = $_POST["id"];
                                        $fullname = ($_POST['fullname']);
                                        $username = ($_POST['username']);
                                        $phoneno = ($_POST['phoneno']);
                                        $_SESSION['username'] = $username;
                                        $result=mysqli_query($connection,"UPDATE rjstrdb SET fullname='$fullname', username='$username',phoneno='$phoneno' WHERE id='$id'")or die(mysqli_connect_error());
                                        if(!$result) 
                                        {
                                        echo "error";
                                        }
                                    }

            
                                    $query="select * from rjstrdb where username='$username'";
                                    $result=mysqli_query($connection,$query);
                                    $row=mysqli_fetch_array($result);
                                
                                    ?>
                                    <div  class=" row userprofile pt-3  mt-3 col-md-12 text-center mr-auto ml-auto ">
                                        <div class="col-md-6">
                                            <form method="post" action="update.php" enctype="multipart/form-data">
                                            
                                                <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                                                    <?php if( $row['image'] != null){?>
                                                    <img src="../images/<?php echo($row['image']);?>" class="images rounded-circle">
                                                    <h5><?php
                                                        error_reporting(0);
                                                        echo $_SESSION['picmsg'];
                                                        $_SESSION['picmsg'] = '';
                                                        'br';
                                                    ?></h5>
                                                    <span class="smallimg"> <input  type="file" name="avatar" required><br><br>
                                                        <input type="submit" name="upload" class="btn btn-md btn-success" value="Update Photo">
                                                    </span>
                                                <?php }else{ ?>
                                                
                                                    <i class="images fa fa-user "></i><br>
                                                    <span class="smallimg"> <input  type="file" name="avatar" required><br><br>
                                                        <input type="submit" name="upload" class="btn btn-md btn-success" value="Upload Photo">
                                                    </span>
                                            
                                                <?php } ?>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                            <form method="post" >
                                                <input type="hidden" name="id" value="<?php echo $row["id"]; ?>">
                                                <div class="row mb-2">
                                                    <div class="col-md-6 "><p>System ID : <?php echo $row['regno'];?></p></div>
                                                    <div class="col-md-6 ">
                                                            <p>Profession : <?php echo $row['profession'];
                                                        $_SESSION["profession"]=$row['profession'];?></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-md-12 mb-2"><p>Fullname : <input type="text" name="fullname"
                                                     value="<?php echo $row['fullname'];?>" pattern=".{3,20}" title="fullname need be more than 2 characters"  required></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-md-12 ">
                                                        <p> Phone No. : <input type="text" name="phoneno" 
                                                        value="<?php echo $row['phoneno'];?>" pattern=".{13,13}" title="enter a valid phone number" required></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-2">
                                                    <div class="col-md-12">
                                                            <p>Email : <input type="email" name="username" 
                                                            value="<?php echo $row['username'];?>" required></p>
                                                    </div>
                                                </div>
                                                <div class="row mb-2 ml-3">
                                                    <div class="col-md-12 mb-5">
                                                        <button class="btn btn-danger btn-md" type="reset" name="cancel">Cancel</button>
                                                        <button class="btn btn-success btn-md" type="submit" name="submit"> Update </button>
                                                    </div>
                                                </div>
                                            
                                            </form>
                                        </div>
                                    </div>
                                    <?php
                                }
                    
                                else if ($label == 'chatpag'){
                                    $username1=$_SESSION['username1'];
                                    $nquery=mysqli_query($connection,"select * from `chat` WHERE userfrom in ('$username1','$username')  and  userto in ('$username1','$username') ORDER BY id ASC   $limit");
                                    ?>
                    
                                    <br>
                                    <form METHOD="POST">
                                        <?php
                                            if(isset($_POST['submit'])){
                                            $msg=$_POST['msg'];
                                            $query = "INSERT INTO chat (userfrom,userto,msg) VALUE ('$username','$username1','$msg')";
                                            $result = mysqli_query($connection,$query);
                                            if($result)     
                                                { 
                                                echo "<script language='JavaScript'>
                                                alert('MSG SENT SUCCESSFULLY')
                                                window.location.href='admincon.php';
                                                </script>";

                                                } 
                                            }
                                        ?>

                                        <div class="chat text-left " id="chat">
                                            
                                            <?PHP
                                            $nqueryUser=mysqli_query($connection,"select * from `rjstrdb` WHERE username ='$username1' ");
                                            $crowUser = mysqli_fetch_array($nqueryUser);
                                            if ( $crowUser['image'] != ''){
                                                ?> 
                                                <img src="../images/<?php echo($crowUser['image']);?>" class="rounded-circle" style="width:60px;height:60px;float:left;">
                                            <?php  }else{
                                                    ?> 
                                                    <i style="font-size:50px;color:black;" class=" fa fa-user "></i>
                                                    <?php
                                                }?>
                                                <br>
                                                <?php echo $username1;
                                                ?><br><br><br> 
                                            <?php
                                                while($crow = mysqli_fetch_array($nquery)){
                                                    if($crow['userfrom']==$username){
                                                        $sender="mine";
                                                    }else{
                                                        $sender="yours";
                                                    }
                                                            
                                                            ?>
                                                <div class="<?php echo $sender ?> messages animated"> 
                                                    <div class="message last">
                                                        <?php echo $crow['msg'] ?>
                                                    </div>
                                                </div>
                                                <?php
                                                    } 
                                                ?>
                                        </div>
                                            <br>Send a message to: <?PHP echo $username1 ?><input type="text" name="msg"  /required> <br><br>
                                            <input id="button" type="submit" name="submit" value="submit">
                                        
                                    </form>
                            
                                    <?php
                                } 
                    
                                
                                else if ($label == 'addadm') {
                                        $page_rows = 5; 
                                        $query=mysqli_query($connection,"select count(id) from `rjstrdb` WHERE usertyp='user' and profession='lecturer' and status='approved' ");
                                        include "pages.php";
                                        $nquery=mysqli_query($connection,"select * from rjstrdb where usertyp='user' and profession='lecturer' and status='approved'   $limit");
                                        ?>
                                        <form METHOD="POST">
                                            <?php
                                            if(isset($_POST['submit'])){ 
                                                ?>
                                                <td><input type="text" name="lect" minlength="3" maxlength="20" /required placeholder="enter lecturer's id"></td>
                                                <td><input type="submit" name="submit" value="SEARCH"/><br><br><br></td>

                                                <?php 
                                                $lect=$_POST['lect'];
                                                $query= ("SELECT * FROM rjstrdb WHERE regno='$lect' and usertyp='user' and profession='lecturer' and status='approved'");
                                                $result=mysqli_query($connection,$query);
                                                ?>

                                                <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                                    <tr><th colspan='7' >APPROVED LECTURER: <?php echo $lect; ?> </th></tr>
                                                    <tr><th></th> <th>Fullname</th> <th>User-id</th>
                                                        <th>Username</th> <th>Phone No</th> <th>Add as an admin.</th> </tr>

                                                            <?php
                                                            while($crow = mysqli_fetch_array($result))
                                                            {
                                                        ?>
                                                    <tr>
                                                        <td> <?php if ( $crow['image'] != ''){
                                                            ?> 
                                                                <img src="../images/<?php echo($crow['image']);?>" class="rounded-circle" style="width:80px;height:80px;">
                                                            <?php  }else{
                                                                ?> 
                                                                <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                                    <?php
                                                            } ?>
                                                        </td>
                                                        <td> <?php echo $crow['fullname'] ?></td>
                                                        <td> <?php echo $crow['regno'] ?></td>
                                                        <td> <?php echo $crow['username'] ?></td>
                                                        <td> <?php echo $crow['phoneno'] ?></td>
                                                        <td align="center"><a href="new.php?label=promote&&id=<?php echo $crow["id"]; ?>" class="link">
                                                            <img src="../images/promote.jpg"  style="width:150px;height:42px;border:0">
                                                            </a></td> 
                                                    </tr>
                                                                    <?php
                                                                }?>
                                                    <tr><td colspan="7" align="right"><a href="newadmin.php"><p2>BACK</p2></a></td></tr>
                                                </table>
                                                    <?php } else {
                                                        ?>
                                                <td><input type="text" name="lect" minlength="3" maxlength="20" /required placeholder="enter lecturer's id"></td>
                                                <td><input type="submit" name="submit" value="SEARCH"/></td>
                                                <br>
                                                    

                                                    <div id="pages"><?php echo $paginationCtrls; ?></div>

                                                    <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                                        
                                                        <tr><th></th><th>Fullname</th><th>User-id</th>
                                                            <th>Username</th><th>Phone no.</th><th>Add as an admin.</th></tr>
                                                            <?php
                                                                $bg = 0;
                                                            while($crow = mysqli_fetch_array($nquery)){
                                                                if ( $bg%2 == 0){
                                                                    $class="light";
                                                                }else{
                                                                    $class="even"; 
                                                                }
                                                                $bg++;
                                                            ?>
                                                            <tr class="<?php echo $class; ?>">
                                                                <td> <?php if ( $crow['image'] != ''){
                                                                    ?> 
                                                                        <img src="../images/<?php echo($crow['image']);?>" class="rounded-circle" style="width:50px;height:50px;">
                                                                    <?php  }else{
                                                                        ?> 
                                                                        <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                                            <?php
                                                                    } ?>
                                                                </td>
                                                                <td> <?php echo $crow['fullname'] ?></td>
                                                                <td> <?php echo $crow['regno'] ?></td>
                                                                <td> <?php echo $crow['username'] ?></td>
                                                                <td> <?php echo $crow['phoneno'] ?></td>
                                                            <td align="center"><a href="new.php?label=promote&&id=<?php echo $crow["id"]; ?>">
                                                                <img src="../images/promote.jpg"  style="width:150px;height:42px;border:0">
                                                                </a></td>

                                                        </tr>
                                                                    
                                                        <?php
                                                        }}
                                                        ?>
                                                    </table>
                                        </form>

                                    <?php 
                                }                       
                    
                                else if ($label == 'otherAdmin'){
                                                        ?>
                                    <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                        <th colspan="7"><p5>OTHER SYSTEM ADMINS</p5></th>
                                        <tr><th>Fullname</th><th>User-id</th><th>Username</th><th>Phone no.</th><th>Admin appoited by:</th><th></th></tr>
                                        <?php
                                            $newquery="select * from rjstrdb where username ='$username'";
                                            $newresult=mysqli_query($connection,$newquery);
                                            $newrow=mysqli_fetch_array($newresult);
                                            $senrAdmin=$newrow['aptdby'];
                                            
                                            $query="select * from rjstrdb where usertyp='admin' and username!='$username'";
                                            $result=mysqli_query($connection,$query);
                                                $bg = 0;
                                            while($row=mysqli_fetch_array($result)){
                                                if ( $bg%2 == 0){
                                                    $class="light";
                                                }else{
                                                    $class="even"; 
                                                }
                                                $bg++;
                                                ?>
                                                <br>


                                                <tr class="<?php echo $class; ?>">
                                                    <td> <?php if ( $row['image'] != ''){
                                                        ?> 
                                                            <img src="../images/<?php echo($row['image']);?>" class="rounded-circle" style="width:50px;height:50px;">
                                                        <?php  }else{
                                                            ?> 
                                                            <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                                <?php
                                                        } ?><br>
                                                    <?php echo $row['fullname'] ?></td>
                                                    <td> <?php echo "&nbsp".$row['regno'] ?></td>
                                                    <td> <?php echo "&nbsp".$row['username'] ?></td>
                                                    <td> <?php echo "&nbsp".$row['phoneno'] ?></td>
                                                    <td> <?php echo $row['aptdby'] ?></td>
                                                    <td>
                                                        <?php if ( $row['username'] == $senrAdmin || $row['username'] == 'jackie@gmail.com'){?>
                                                            <i style="font-size:35px;color:black;" class=" fa fa-user-lock "></i>
                                                            
                                                        <?php }else{
                                                            ?>   
                                                        
                                                            <a href="new.php?label=rmvadm&&id=<?php echo $row["id"]; ?>">
                                                                <img src="../images/demote.jpg"  style="width:150px;height:42px;border:0">
                                                            </a>
                                                            <?php
                                                        } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            <tr><td colspan='7' class="text-primary"><a href="new.php?label=addadm">
                                                <i style="font-size:25px" class="fa fa-plus"></i>
                                                ADD AN ADMIN</a></td>
                                            </tr>
                                        </table>                       
                                    <?php 
                                }  
                                
                    
                                else if ($label == 'ad-user' ) {
                                        $page_rows = 5; 
                                        $query=mysqli_query($connection,"select count(id) from `rjstrdb` WHERE usertyp='user'  ");
                                        include "pages.php";
                                        $nquery=mysqli_query($connection,"select * from rjstrdb where usertyp='user'   $limit");
                                        ?>
                                    <form METHOD="POST">
                                        <?php
                                        if(isset($_POST['submit'])){ 
                                        ?>

                                        <br>
                                        <td><input type="text" name="lect" minlength="3" maxlength="20" /required placeholder="enter lecturer's id"></td>
                                        <td><input type="submit" name="submit" value="SEARCH"/><br><br><br></td>

                                        <?php 
                                            $lect=$_POST['lect'];
                                            $query= ("SELECT * FROM rjstrdb WHERE regno='$lect' and usertyp='user' ");
                                            $result=mysqli_query($connection,$query);
                                        ?>

                                        <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                            <tr><th>Fullname</th><th>Profession</th><th>User-Id</th>
                                            <th>Username</th><th>Phone No</th><th></th></tr>

                                                <?php
                                                    while($crow = mysqli_fetch_array($result))
                                                {
                                            ?>
                                            <tr >
                                                
                                                <td> 
                                                     <?php if ( $crow['image'] != ''){
                                                        ?> 
                                                            <img src="../images/<?php echo($crow['image']);?>" class="rounded-circle" style="width:80px;height:80px;">
                                                        <?php  }else{
                                                            ?> 
                                                            <i style="font-size:35px;color:black;width:30px;height:30px;" class="fa fa-user"></i>
                                                                <?php
                                                        }  echo $crow['fullname'] ?>
                                                </td>
                                                <td><?php echo $crow['profession'] ?></td>
                                                <td> <?php echo $crow['regno'] ?></td>
                                                <td> <?php echo $crow['username'] ?></td>
                                                <td> <?php echo $crow['phoneno'] ?></td>


                                                <td> <?php if ( $crow['status'] == 'approved'){?>
                                                    <a href="new.php?label=demoteuser&&id=<?php echo $crow["id"]; ?>&&profession=<?php echo $crow["profession"]; ?>" class="link">
                                                    <i style="font-size:12px;color:blue;">This is an approved user</i>
                                                    <i style="font-size:20px;color:red;">Ban</i>
                                                    </a>  
                                                        <?php  }else{
                                                            ?> 
                                                            <a href="new.php?label=acceptUser&&id=<?php echo $crow["id"]; ?>&&profession=<?php echo $crow["profession"]; ?>" class="link">
                                                            <i style="font-size:12px;color:red;">This is a disapproved user</i>
                                                            <i style="font-size:20px;color:blue;"> Approve</i>
                                                                <?php
                                                        } ?>
                                                </td>
                                            </tr>
                                            <?php
                                                }
                                            ?>
                                        </table>
                                        <?php } else
                                        {
                                        ?>
                                        <br>
                                        <td><input type="text" name="lect" minlength="3" maxlength="20" /required placeholder="enter lecturer's id"></td>
                                        <td><input type="submit" name="submit" value="SEARCH"/></td>
                                        

                                        <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                            
                                            <tr><th colspan="6"><p5>System Users</p5></th></tr>
                                            <tr><th>Fullname</th><th>Profession</th><th>User-Id</th>
                                            <th>Username</th><th>Phone No</th><th>Ban/Approve User</th></tr>
                                                <?php
                                                    $bg = 0;
                                                    while($crow = mysqli_fetch_array($nquery)){
                                                        if ( $bg%2 == 0){
                                                            $class="light";
                                                        }else{
                                                            $class="even"; 
                                                        }
                                                        $bg++;
                                                ?>
                                            <tr class="<?php echo $class; ?>">
                                                
                                                <td style="text-align:left"> 
                                                     <?php if ( $crow['image'] != ''){
                                                        ?> 
                                                            <img src="../images/<?php echo($crow['image']);?>" class="rounded-circle" style="width:50px;height:50px;">
                                                        <?php  }else{
                                                            ?> 
                                                            <i style="font-size:35px;color:black;width:20px;height:20px;padding-right:50px;" class="fa fa-user"></i>
                                                                <?php
                                                        }  echo $crow['fullname'] ?>
                                                </td>
                                                <td><?php echo $crow['profession'] ?></td>
                                                <td> <?php echo $crow['regno'] ?></td>
                                                <td> <?php echo $crow['username'] ?></td>
                                                <td> <?php echo $crow['phoneno'] ?></td>
                                                <td>
                                                    
                                                    <?php if ( $crow['status'] == 'approved'){?>
                                                    <a style="text-decoration:none" href="new.php?label=demoteuser&&id=<?php echo $crow["id"]; ?>&&profession=<?php echo $crow["profession"]; ?>" class="link">
                                                    <i style="font-size:12px;color:blue;">This is an approved user</i>
                                                    <i style="font-size:20px;color:red;">Ban</i>
                                                    </a>  
                                                        <?php  }else{
                                                            ?> 
                                                            <a style="text-decoration:none" href="new.php?label=acceptUser&&id=<?php echo $crow["id"]; ?>&&profession=<?php echo $crow["profession"]; ?>" class="link">
                                                            <i style="font-size:12px;color:red;">This is a disapproved user</i>
                                                            <i style="font-size:20px;color:blue;"> Approve</i>
                                                                <?php
                                                        } ?>
                                                </td>
                                            </tr>
                                                <?php
                                                }?>
                                                 </table>
                                                    <div id="pages"><?php echo $paginationCtrls; ?></div>
                                                <?php }
                                                ?>
                                       
                                    </form>

                                    <?php 
                                }   
                                                        
                               
                    
                                else if ($label == 'u-units') {
                                    $page_rows = 5; 
                                    $query=mysqli_query($connection,"select count(id) from `units` WHERE LECTURER ='' and status='' ");
                                    include "pages.php";
                                    $nquery=mysqli_query($connection,"SELECT * FROM units WHERE LECTURER ='' and status=''   $limit");
                                    ?>
                                  
                                    <form METHOD="POST">
                                        <br> <br>
                                        <table border="1" cellspacing="0" cellpadding="1" width="60%" align="center">
                                            <tr><th colspan='6' ><p5>UNALLOCATED UNITS</p5></th></tr>
                                            <tr><th>Student-Year</th><th>Unit Code</th><th>Unit Name</th><th>Assign A Lecturer</th></tr>
                                            <tr><?php echo $paginationCtrls; ?></tr>
                                                    <?php
                                                        $bg = 0;
                                                        while($crow = mysqli_fetch_array($nquery)){
                                                            if ( $bg%2 == 0){
                                                                $class="light";
                                                            }else{
                                                                $class="even"; 
                                                            }
                                                            $bg++;
                                                            
                                                        ?>
                                            <tr class="<?php echo $class; ?>">
                                                <td> <?php echo $crow['year'] ?></td>
                                                <td> <?php echo $crow['unitcode'] ?></td>
                                                <td> <?php echo $crow['unitname'] ?></td>
                                                <td align="center"><a href="new.php?label=allocate&&id=<?php echo $crow["id"]; ?>&&unitcode=<?php echo $crow["unitcode"]; ?>" class="link">
                                                    <img src="../images/edit.jpg"  style="width:55px;height:42px;border:0">
                                                    </a></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </table>
                                    </form>
                                    <?php
                                }   
                    
                                else if ($label == 'allocate'){
                                    $unitcode = $_SESSION["unitcode"];
                                    
                                    ?>
                                    <form METHOD="POST">
                                        <table border="1" cellspacing="0" cellpadding="1" width="60%" align="center">
                                            <tr><th colspan='4' ><?php echo $unitcode ?></th></tr>
                                            <tr><th>Lec' Username</th><th>Select Lecturer</th></tr>
                                            <?phP
                                            $query= ("SELECT * FROM rjstrdb WHERE profession='lecturer' and usertyp='user' and status='approved'")or die(mysqli_connect_error()); 
                                            $result=mysqli_query($connection,$query);
                                            while($row = mysqli_fetch_array($result))
                                            {
                                                ?>
                                                <tr>
                                                    <td> <?php if ( $row['image'] != ''){
                                                            ?> 
                                                                <img src="../images/<?php echo($row['image']);?>" class="rounded-circle" style="width:80px;height:80px;">
                                                            <?php  }else{
                                                                ?> 
                                                                <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                                    <?php
                                                            } ?>
                                                        
                                                        <?php echo $row['username'] ?>
                                                    </td>
                                                    <td><a href="new.php?label=asynlec&&newname=<?php echo $row["username"]; ?>" class="link"style="font-size:30px">&#10004</a></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </form>

                                    
                                    <?php
                                }    
                                                        
                                else if ($label == 'eachUnit') {
                                    $lecname = $_SESSION["lecname"];
                                    $page_rows = 5;
                                    $query=mysqli_query($connection,"select count(id) from `units` WHERE LECTURER = '$lecname' ");
                                    include "pages.php";
                                    $nquery=mysqli_query($connection,"SELECT * FROM units WHERE LECTURER = '$lecname'   $limit");
                                    ?>
                                     <br>
                                    <table border="1" align="center" width="60%">
                                        <th colspan=5> ASSYNED UNITS TO: <?PHP echo $lecname ?> </th>
                                        <tr><th>Unit Code</th><th>Unit Name</th><th>Year Of Study</th></tr>
                                        <tr><?php echo $paginationCtrls; ?></tr>
                                            <?php
                                            while($crow = mysqli_fetch_array($nquery)){
                                                ?>
                                        <tr>
                                            <td><?php echo $crow['unitcode'];?></td>
                                            <td><?php echo $crow['unitname'];?></td>
                                            <td><?php echo $crow['year'];?></td>

                                            <td><a href="new.php?label=lec&&id=<?php echo $crow["id"]; ?>&&unitcode=<?php echo $crow["unitcode"]; ?>" class="link">ASSIGN A DIFFERENT LECTURER</a></td>
                                        </tr>

                                        <?php } ?>

                                    </table>
                                    
                                    <?php
                                } 
                                                        
                                else if ($label == 'a-units' ){
                                    if(isset($_POST['submit'])){ 

                                        $year=$_POST['year']; 
                                        $_SESSION["year"] = "$year";
                                        $_SESSION["semester"] = "$semester";
                                    ?>

                                    <form METHOD="POST">
                                        <?php
                                        $lecname = $_SESSION["lecname"];
                                        $page_rows = 6;
                                        $query= mysqli_query($connection,"SELECT count(id) FROM units WHERE year='$year'  AND LECTURER !='' and status='' ")or die(mysqli_connect_error()); 
                                        include "pages.php";
                                        $nquery=mysqli_query($connection,"SELECT * FROM units WHERE year='$year'  AND LECTURER !='' and status='' $limit")or die(mysqli_connect_error()); 
                                        ?>
                                                                            
                                        <input type="number" name="year" minlength="3" maxlength="20" placeholder="year" /required>
                                        <td><input type="submit" name="submit" value="SEARCH"/></td>
                                                <br>
                                        <table border="1" cellspacing="0" cellpadding="3" width="60%" align="center">
                                            <tr><th colspan='6' ><P5><?php echo 'STUDENT-YEAR :-  '; echo $year; ?></P5></th></tr>
                                            <tr><th>Student-Year</th><th>Unit Code</th>
                                            <th>Unit Name</th><th>Assigned Lecturer</th><th>Change The Lecturer</th></tr>


                                                
                                                <?php
                                                    $bg = 0;
                                                    while($crow = mysqli_fetch_array($nquery)){
                                                        if ( $bg%2 == 0){
                                                            $class="light";
                                                        }else{
                                                            $class="even"; 
                                                        }
                                                        $bg++;
                                                ?>
                                            <tr class="<?php echo $class; ?>">
                                                <td><?php echo $crow['year']?></td>
                                                <td> <?php echo $crow['unitcode'] ?></td>
                                                <td> <?php echo $crow['unitname'] ?></td>
                                                <td> <?php echo $crow['LECTURER'] ?></td>
                                                <td><a href="new.php?label=changeLec&&id=<?php echo $crow["id"]; ?>&&unitcode=<?php echo $crow["unitcode"]; ?>" >
                                                <i class="fa fa-edit" style="font-size:20px;color:blue"></i></a><br></br></td>
                                            </tr>
                                                        <?php
                                                    }

                                                ?>
                                        </table>
                                        <div id="pages"><?php echo $paginationCtrls; ?></div>
                                                    
                                    </form>
                                    <?php } else { 
                                         $page_rows = 6;
                                         $query=mysqli_query($connection,"select count(id) from `units` WHERE LECTURER !='' and status=''");
                                         include "pages.php";
                                         $nquery=mysqli_query($connection,"SELECT * FROM units WHERE LECTURER !='' and status=''  $limit");
                                         ?>
           
                                    <form  METHOD="POST">
                                        
                                        <input type="number" name="year" minlength="3" maxlength="20" placeholder="year" /required>
                                        <td><input type="submit" name="submit" value="SEARCH"/></td> <br> <br>
                                        <table border="1" cellspacing="1" cellpadding="3" width="60%" align="center">
                                            <th colspan='7'  ><p5>ALLOCATED UNITS<p5></th>
                                                    <tr><th >Student-Year</th><th>Unit's System Code</th><th>Unit Code</th>
                                            <th>Unit Name</th><th>Assigned Lecturer</th><th>Choose A Different Lecturer</th></tr>

                                                <?php
                                                    $bg = 0;
                                                    while($crow = mysqli_fetch_array($nquery)){
                                                        if ( $bg%2 == 0){
                                                            $class="light";
                                                        }else{
                                                            $class="even"; 
                                                        }
                                                        $bg++;

                                                    ?>
                                            <tr class="<?php echo $class; ?>">
                                                    <td><?php echo $crow['year'] ?></td>
                                                    <td><?php echo $crow['idunit']; ?></td>
                                                    <td><?php echo $crow['unitcode']; ?></td>
                                                    <td><?php echo "&nbsp".$crow['unitname']; ?></td>
                                                    <td><?php echo $crow['LECTURER']; ?></td>

                                                    <td align="center"><a href="new.php?label=changeLec&&id=<?php echo $crow["id"]; ?>&&unitcode=<?php echo $crow["unitcode"]; ?>" >
                                                    <i class="fa fa-edit" style="font-size:20px;color:blue"></i>
                                                    </a></td>
                                            </tr>
                                            <?php
                                                }		
                                            ?>
                                        </table>
                                        <div id="pages"><?php echo $paginationCtrls; ?></div>
                                    </form>

                                    <?php }
                                }  
                                                        
                                
                                else if ($label == 'changeLec') {
                                    $unitcode = $_SESSION['unitcode'];
                                    ?>
                                    <form METHOD="POST">
                                            <br>
                                            <table border="1" cellspacing="0" cellpadding="1" width="60%" align="center">
                                                <tr><th colspan='3' ><?php echo $unitcode ?></th></tr>
                                                <tr><th>Lec' Username</th><th>Select Lecturer</th><th>Assgn To The Lecturer</th></tr>
                                                <?php
                                                $query= ("SELECT * FROM rjstrdb WHERE profession='lecturer' and usertyp='user' and status='approved'")or die(mysqli_connect_error()); 
                                                $result=mysqli_query($connection,$query);
                                                while($row = mysqli_fetch_array($result))
                                                {
                                                    ?>
                                                    <tr>
                                                        <td> <?php if ( $row['image'] != ''){
                                                                ?> 
                                                                    <img src="images/<?php echo($row['image']);?>" class="rounded-circle" style="width:35px;height:35px;">
                                                                <?php  }else{
                                                                    ?> 
                                                                    <i style="font-size:35px;color:black;" class=" fa fa-user "></i>
                                                                        <?php
                                                                } ?>
                                                        </td>
                                                        <td> <?php echo $row['username'] ?></td>
                                                        
                                                        <td><a href="new.php?label=asynn&&nname=<?php echo $row["username"]; ?>" class="link" style="font-size:30px;color:blue">&#10004</a></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </table>
                                    </form>    
                                    <?php
                                    
                                }                       

                                else if ($label == 'newunits'){
                                        
                                    if(isset($_POST['submit'])){ 

                                        $year=$_POST['year'];
                                        $semester=$_POST['semester']; 
                                        $_SESSION["year"] = "$year";
                                        $_SESSION["semester"] = "$semester";
                                    ?>

                                    <form METHOD="POST">
                                        <?php
                                          $page_rows = 5;
                                          $query= mysqli_query($connection,"SELECT count(id) FROM units WHERE year='$year'  and status='' ")or die(mysqli_connect_error()); 
                                          include "pages.php";
                                          $nquery=mysqli_query($connection,"SELECT * FROM units WHERE year='$year'  and status='' $limit")or die(mysqli_connect_error()); 
                                          ?>
                                        
                                        <br>
                                        <input type="number" name="year"   minlength="3" maxlength="20" placeholder="year" /required>
                                        <td><input type="submit" name="submit" value="SEARCH"/></td>
                                                <br>
                                        <table border="1" cellspacing="0" cellpadding="1" width="100%" align="center">
                                            <tr><th colspan='9' ><P5><?php echo 'STUDENT-YEAR:-'; echo $year; ?></P5></th></tr>
                                                <th>Student-Year</th><th>Unit Code</th><th>Unit Name</th><th>Assigned Lecturer</th><th>Remove From The Syllabus</th><th>Edit The Unit</th>


                                                <div id="pages"><?php echo $paginationCtrls; ?></div>
                                                <?php
                                                while($crow = mysqli_fetch_array($nquery)){
                                                    ?>
                                            <tr>
                                                <td><?php echo $crow['year']; ?></td>
                                                <td> <?php echo $crow['unitcode'] ?></td>
                                                <td> <?php echo $crow['unitname'] ?></td>
                                                <td> <?php echo $crow['LECTURER'] ?></td>
                                                <td><a href="new.php?label=delUnit&&id=<?php echo $crow["id"]; ?>&&year=<?php echo $crow["year"]; ?>&&semister=<?php echo $crow["semester"];?>" class="link">
                                                        <i class="fa fa-trash" style="font-size:20px;color:red"></i>
                                                            </a></td>
                                                        <td><a href="new.php?label=editUnit&&id=<?php echo $crow["id"]; ?>&&year=<?php echo $crow["year"]; ?>&&semister=<?php echo $crow["semester"];?>" class="link">
                                                        <i class="fa fa-edit" style="font-size:20px;color:blue"></i>
                                                            </a><br><br></td>
                                            </tr>
                                                <?php
                                                        }

                                                    ?>
                                        </table>
                                                    
                                    </form>
                                        <?php } else { 
                                            $page_rows = 6;
                                            $query=mysqli_query($connection,"select count(id) from `units` WHERE status='' ");
                                            include "pages.php";
                                            $nquery=mysqli_query($connection,"SELECT * FROM units WHERE status=''  $limit");
                                            ?>
                                    
                                            <form METHOD="POST">
                                                

                                                <div id="add" style="color:blue">
                                                    <a style="float:left;" href="new.php?label=new-unit" >
                                                    <i class="fa fa-plus" style="font-size:30px"></i>
                                                    ADD A NEW UNIT</a>
                                                </div>
                                                <input type="number" name="year" minlength="3" maxlength="20" placeholder="year" /required>
                                                <td><input type="submit" name="submit" value="SEARCH"/></td>
                                                <br> 
                                                <br>

                                                <table border="1" cellspacing="0" cellpadding="3" width="100%" align="center">
                                                    <tr><th colspan='8' ><p5>SYSTEM UNITS</p5></th></tr>
                                                    <tr><th>Unit's System-code</th><th>Student-Year</th><th>Unit- Code</th><th>Unit Name</th><th>Assigned Lecturer</th><th>Remove From The Syllabus</th><th>Edit The Unit</th></tr>
                                                        <?php
                                                            $bg = 0;
                                                            while($crow = mysqli_fetch_array($nquery)){
                                                                if ( $bg%2 == 0){
                                                                    $class="light";
                                                                }else{
                                                                    $class="even"; 
                                                                }
                                                                $bg++;

                                                        ?>
                                                    <tr class="<?php echo $class; ?>">
                                                        <td> <?php echo $crow['idunit'] ?></td>
                                                        <td> <?php echo $crow['year'] ?></td>
                                                        <td> <?php echo $crow['unitcode'] ?></td>
                                                        <td> <?php echo $crow['unitname'] ?></td>
                                                        <td> <?php echo $crow['LECTURER'] ?></td>
                                                        <td><a href="new.php?label=delUnit&&id=<?php echo $crow["id"]; ?>&&year=<?php echo $crow["year"]; ?>&&semister=<?php echo $crow["semester"];?>" class="link">
                                                        <i class="fa fa-trash" style="font-size:20px;color:red"></i>
                                                            </a></td>
                                                        <td><a href="new.php?label=editUnit&&id=<?php echo $crow["id"]; ?>&&year=<?php echo $crow["year"]; ?>&&semister=<?php echo $crow["semester"];?>" class="link">
                                                        <i class="fa fa-edit" style="font-size:20px;color:blue"></i>
                                                            </a></td>
                                                    </tr>
                                                        <?php
                                                        }
                                                    ?>

                                                </table>
                                                <div id="pages"><?php echo $paginationCtrls; ?></div>
                                            </form>

                                            <?php }
                                }
                                        
                                else if ($label == 'editUnit') {

                                    if(isset($_POST['submit'])){ 

                                        $year=$_POST['year'];
                                        $semister=$_POST['semister'];
                                        $unitcode=$_POST['unitcode'];
                                        $unitname=$_POST['unitname'];
                                        $idunit=$_POST['idunit'];

                                        $_SESSION["semister"] = "$semister"; 
                                        $_SESSION["year"] = "$year";


                                        $result=mysqli_query($connection,"UPDATE units SET year='$year', semester='$semister',unitcode='$unitcode',unitname='$unitname' where idunit='$idunit' ");

                                        if($result){ 
                                            $_SESSION["label"] ="newunits";
                                        echo "<script language='JavaScript'>
                                            alert('UNIT HAS BEEN WELL EDITED')
                                            window.location.href='admincon.php';
                                            </script>";
                                                    }    
                                            else{
                                                echo "an error occured";
                                            }
                                        }
                                        ?>



                                        <form method="post">
                                            <?php

                                            $id= $_SESSION['id'];
                                            $query="select * from units where id=$id";
                                            $result=mysqli_query($connection,$query);
                                            $row=mysqli_fetch_array($result);
                                            if($result){
                                                $year=$row["year"];
                                                $semester=$row["semester"];
                                                $unitcode=$row["unitcode"];
                                                $unitname=$row["unitname"];
                                                $idunit=$row["idunit"];
                                                ?>

                                                <br><br>
                                                <span style="font-size:20px;color:red;">*all fields are required</span>
                                                <table border="1"  cellpadding="10" width="40%" align="center">
                                                    <th colspan="2"><?php echo $idunit ?></th>
                                                        <input type="hidden" name="idunit" value="<?php echo $idunit ?>">
                                                    <tr><td>YEAR</td> <td> <input type="text" value="<?php echo $year ?>" name="year">  </td></tr>
                                                    <tr><td>SEMISTER</td>   <td> <input type="text" value="<?php echo $semester ?>" name="semister"></td></tr>
                                                    <tr><td>UNIT CODE: </td>   <td><input type="text" value="<?php echo $unitcode ?>" name="unitcode"></td></tr>
                                                    <tr><td>UNIT NAME:</td> <td><input type="text" value="<?php echo $unitname ?>" name="unitname"></td></tr>
                                                        <tr><td><input id="button" type="reset" class="btn btn-danger" name="cancel" value="cancel"></td>
                                                            <td><input id="button" type="submit" class="btn btn-success" name="submit" value="submit"></td> 
                                                            
                                                        </tr>
                                                </table>
                                        </form>
                                            <?php
                                            }
                                }  
                    

                                else if ($label == 'new-unit'){
                                    if(isset($_POST['submit'])){ 
                                        $year=$_POST['year'];
                                        $semister=$_POST['semister'];
                                        $unitcode=$_POST['unitcode'];
                                        $unitname=$_POST['unitname'];

                                        $_SESSION["semister"] = "$semister"; 
                                        $_SESSION["year"] = "$year";
                                        $_SESSION["label"] = "newunits";
                                        $query = "INSERT INTO units (year,semester,unitcode,unitname,LECTURER,status,idunit ) VALUES ('$year','$semister','$unitcode','$unitname','','','')";
                                        $result = mysqli_query($connection,$query);    
                                        if($result){ 
                                            $last_id = $connection->insert_id;
                                            $idunit= "unit-001" . $last_id;
                                            $result4=mysqli_query($connection,"UPDATE units SET idunit='$idunit' WHERE id='$last_id'");

                                            echo "<script language='JavaScript'>
                                            alert('UNIT HAS BEEN ADDED SUCCESSFULLY')
                                            window.location.href='new.php?label=new-unit';
                                            </script>";	
                                            exit;         }    
                                            else{
                                                echo "an error occured";
                                            }
                                    }
                                        ?>

                                        <form method="POST">
                                            <span class="text-danger" style="font-size:20px;">*all fields are required</span>
                                            <table border="1"  cellpadding="10" width="40%" align="center">
                                                <th colspan="2">ADD A UNIT TO THE SYLLABUS</th>
                                                <tr><td>YEAR</td> <td>   <select name="year" REQUIRED>
                                                    <option value="">SELECT YEAR</option>
                                                    <option value="1">FIRST YEAR</option>
                                                    <option value="2">SECOND YEAR</option>
                                                    <option value="3">THIRD YEAR</option>
                                                    <option value="4">FOURTH YEAR</option></select></td></tr>
                                                <tr><td>SEMISTER</td>   <td><select name="semister" REQUIRED>
                                                        <option value="">SELECT SEMISTER</option>
                                                        <option value="1">FIRST SEMISTER</option>
                                                        <option value="2">SECOND SEMISTER</option> </td></tr>
                                                <tr><td>UNIT CODE: </td>   <td><input type="text"  name="unitcode" minlength="3" maxlength="20" placeholder="eg. BICT 111" /required></td></tr>
                                                <tr><td>UNIT NAME:</td> 
                                                    <td><textarea placeholder="eg. ICT AND SOCIETY" name="unitname" col="7" rows="2" /required> </textarea></td></tr>
                                                    <tr><td><input id="button" class="btn btn-danger" type="reset" name="cancel" value="cancel"></td>
                                                        <td><input id="button" class="btn btn-success" type="submit" name="submit" value="submit"></td> 
                                                        
                                                    </tr>
                                            </table>
                                        </form>   
                                    <?php 
                                } 
                    
                                else if ($label == 'lec'){
                                    $unitcode = $_SESSION['unitcode'];
                                    ?>
                                    <form method="post">
                                        <table border="1" cellspacing="0" cellpadding="1" width="60%" align="center">
                                            <th colspan='4'><?php echo $unitcode ?></th>
                                            <tr><th>LEC' Username</th><th>Select Lecturer</th></tr>
                                                <?php
                                                $query= ("SELECT * FROM rjstrdb WHERE profession='lecturer' and usertyp='user' and status='approved'")or die(mysqli_connect_error()); 
                                                $result=mysqli_query($connection,$query);
                                                while($row = mysqli_fetch_array($result))
                                                {
                                                    ?>
                                            <tr><td> <?php echo $row['username'] ?></td>
                                                <td><a href="new.php?label=asyn&&newName=<?php echo $row["username"]; ?>" class="link">ASSYN TO THE LECTURER</a></td>
                                            </tr>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </form>
                                    
                                    <?php 
                                }                        
                                else if ($label == 'trends'){
                                    include "trend.php";
                                }

                                ?>
                        </div>
                    </div>
                </div>
                    
               
            </div>
    

            </div>
        
        
        <?php include "footer.php"; ?>
    </body>
        <script src="../js/formscript.js"></script>
        <!-- <script>document.getElementById('chat').scrollHeight(); </script> -->
</html>